<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColBeliCoinsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beli_coins', function (Blueprint $table) {
            $table->decimal('coin_saat_ini', 64,2)->default(0,00)->after('coin');
            $table->decimal('coin_berkurang', 64,2)->default(0,00)->after('coin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beli_coins', function (Blueprint $table) {
            $table->dropColumn('coin_saat_ini');
            $table->dropColumn('coin_berkurang');
        });
    }
}
