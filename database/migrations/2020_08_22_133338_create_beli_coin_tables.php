<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeliCoinTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beli_coins', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id_pembeli');
            $table->uuid('user_id_penjual');
            $table->string('faktur')->nullable();
            $table->decimal('coin', 64,2)->default(0,00);
            $table->bigInteger('biaya');
            $table->integer('isActive')->default(0);
            $table->text('upload_bukti')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beli_coins');
    }
}
