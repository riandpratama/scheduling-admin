<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokoPemesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toko_pemesanans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_pemesanan');
            $table->uuid('user_id_pembeli');
            $table->uuid('user_id_penjual');
            $table->integer('provinsi_id');
            $table->string('provinsi');
            $table->integer('kabupaten_id');
            $table->string('kabupaten');
            $table->text('alamat_kirim')->nullable();
            $table->date('tgl_konfirmasi')->nullable();
            $table->string('kurir')->nullable();
            $table->integer('ongkos_kirim')->nullable();
            $table->string('service_kurir')->nullable();
            $table->integer('status')->default(0);
            $table->integer('total_pesan')->default(0);
            $table->decimal('harga_coin', 64,2)->default(0,00);
            $table->integer('harga_rupiah')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toko_pemesanans');
    }
}
