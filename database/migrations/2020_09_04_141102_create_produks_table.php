<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toko_produks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('user_id');
            $table->string('nama_produk');
            $table->text('deskripsi');
            $table->decimal('harga_coin', 64,2)->default(0,00);
            $table->integer('harga_rupiah')->default(0);
            $table->integer('stok')->default(0);
            $table->string('image_sampul');
            $table->string('slug');
            $table->string('reference');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toko_produks');
    }
}
