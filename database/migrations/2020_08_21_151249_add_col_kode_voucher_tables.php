<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColKodeVoucherTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kode_vouchers', function (Blueprint $table) {
            $table->decimal('coin_saat_ini', 64,2)->default(0,00)->after('isActive');
            $table->decimal('coin_diambil', 64,2)->default(0,00)->after('isActive');
            $table->decimal('coin_berkurang', 64,2)->default(0,00)->after('isActive');
            $table->bigInteger('biaya')->after('isActive');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kode_vouchers', function (Blueprint $table) {
            $table->dropColumn('coin_saat_ini');
            $table->dropColumn('coin_diambil');
            $table->dropColumn('coin_berkurang');
            $table->dropColumn('biaya');
        });
    }
}
