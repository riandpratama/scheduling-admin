<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUltracoindIdOnKirimCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kirim_coins', function (Blueprint $table) {
            $table->uuid('ultra_coin_id')->nullable()->after('user_id_penjual');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kirim_coins', function (Blueprint $table) {
            $table->dropColumn('ultra_coin_id');
        });
    }
}
