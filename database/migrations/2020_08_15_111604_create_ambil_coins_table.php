<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmbilCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ambil_coins', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');
            $table->uuid('ultracoin_id');
            $table->date('tgl_mining_terakhir')->nullable();
            $table->decimal('coin_saat_ini', 64,2)->default(0,00);
            $table->decimal('coin_diambil', 64,2)->default(0,00);
            $table->decimal('coin_berkurang', 64,2)->default(0,00);
            $table->bigInteger('biaya');
            $table->integer('isActive')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ambil_coins');
    }
}
