<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUltraCoinIdOnAmbilCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ambil_coins', function (Blueprint $table) {
            $table->string('faktur')->nullable()->after('user_id');
            $table->uuid('ultra_coin_id')->nullable()->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ambil_coins', function (Blueprint $table) {
            $table->dropColumn('faktur');
            $table->dropColumn('ultra_coin_id');
        });
    }
}
