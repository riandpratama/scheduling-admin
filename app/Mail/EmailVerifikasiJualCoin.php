<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailVerifikasiJualCoin extends Mailable
{
    use Queueable, SerializesModels;

    protected $coin;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($coin)
    {
        $this->coin = $coin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.verify-jual-coin')
                ->with(['coin' => $this->coin])
                ->from('support@wallbroz.com');
    }
}
