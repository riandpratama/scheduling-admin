<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'isActive', 'tanggal_berakhir_cloud', 'isRegistration'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }
    
    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    public function userdetail()
    {
        return $this->hasOne('App\Models\UserDetail', 'user_id', 'id');
    }

    public function ultracoin()
    {
        return $this->hasMany('App\Models\UltraCoin', 'user_id', 'id')->orderBy('created_at', 'asc');
    }

    public function sponsor()
    {
        return $this->hasOne('App\Models\Sponsor', 'user_id', 'id');
    }

    public function regisrasiakun()
    {
        return $this->hasOne('App\Models\RegistrasiAkun', 'user_id', 'id');
    }

    public function kodevoucher()
    {
        return $this->hasOne('App\Models\KodeVoucher', 'user_id', 'id');
    }

    public function bonussponspor()
    {
        return $this->hasOne('App\Models\BonusSponsor', 'user_id', 'id');
    }

    public function ultracoindelete()
    {
        return $this->hasOne('App\Models\UltraCoin', 'user_id', 'id');
    }

    public function messageOne()
    {
        return $this->hasOne('App\Models\Message', 'from', 'id')->orderBy('created_at', 'desc');
    }
}
