<?php

namespace App\Console\Commands;

use App\Models\UltraCoin;
use Illuminate\Console\Command;

class MiningCoin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'day:point';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $coin = UltraCoin::where('isActive', '=', 1)
                ->where(function ($query) {
                    $query->orWhereNotNull('topup')->orWhere('coin', '>=', 10);
                })
                ->whereDate('tgl_mining', \Carbon\Carbon::yesterday())
                ->get();
    
        $checkRipening = UltraCoin::where('ripening', '=', 0)
                ->whereDate('tgl_mining', \Carbon\Carbon::yesterday())
                ->get();

        $array = null;
        foreach ($coin as $item){
            $array[] = $item->user_id;
            $array[] = $item->coin;
            $array[] = $item->tgl_mining;
            $array[] = $item->ripening;
            $array[] = $item->topup;
            $array[] = $item->topup_rippening;
            $array[] = $item->topup_temporary;
        }
        $array_chunk = array_chunk($array, 7);
        foreach ($array_chunk as $key => $val){

            if ($array_chunk[$key][3] == 0){
                $coinUp = ($array_chunk[$key][1]*0.2/100) + $array_chunk[$key][1] + $array_chunk[$key][6];
                $topup = NULL;
            } else {
                $coinUp = $array_chunk[$key][1];
                $topup = NULL;
            }

            UltraCoin::create([
                'user_id' => $array_chunk[$key][0],
                'coin' => $coinUp,
                'tgl_mining' => \Carbon\Carbon::now()->format('Y-m-d'),
                'ripening' => $array_chunk[$key][3] > 0 ? $array_chunk[$key][3] - 1 : 0 ,
                'topup' =>  $topup,
                'topup_rippening' => $array_chunk[$key][5] > 0 ? $array_chunk[$key][5] - 1 : 0,
                'topup_temporary' => $array_chunk[$key][4],
                'isActive' => 1
            ]);
        }
    }
}
