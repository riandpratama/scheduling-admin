<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Hash;
use Session;
use App\User;
use Illuminate\Http\Request;
use App\Mail\VerifikasiResetPassword;

class ResetPasswordMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('password_resets')->get();

        return view('reset-password.index', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $token)
    {
        $email = DB::table('password_resets')->where('token', $token)->first()->email;

        $user = User::where('email', $email)->first();
        
        User::where('email', $email)->update(['password'=> Hash::make($email)]);

        DB::table('password_resets')->where('email', $email)->delete();

        Mail::to($email)->send(new VerifikasiResetPassword($email));
        
        Session::flash('success', 'Berhasil, merubah password.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
