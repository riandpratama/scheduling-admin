<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use App\User;
use DataTables;
use Carbon\Carbon;
use App\Models\SewaCloud;
use App\Models\UltraCoin;
use Illuminate\Http\Request;
use App\Mail\VerifikasiUserMail;

class PendaftarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('pendaftar.index');
    }

    public function getData()
    {
        $all = DB::table('users')
                ->select([
                    'users.id', 'username', 'nama_sponsor', 'email', 
                    'nama_lengkap', 'tgl_lahir', 'jenis_kelamin', 'alamat', 
                    'no_hp', 'nama_bank', 'nama_cabang', 'no_rekening', 'swift_code','no_ktp',
                    'upload_foto', 'upload_ktp', 'upload_pembayaran', 'user_details.created_at'
                ])
                ->join('user_details', 'users.id', 'user_details.user_id')
                ->join('ultra_coins', 'users.id', 'ultra_coins.user_id')
                ->where('role', 'user')->whereNotNull('email_verified_at')
                ->where('users.isActive', 0)->where('users.isRegistration', 1)->get();

        $url = 'http://scheduling.localhost/storage';

        return $allDataTables = Datatables::of($all)->addIndexColumn()
                                ->addColumn('image1', function ($item) use ($url) {
                                    return '<img src='."$url".'/'."$item->upload_foto".' width="100"> ';
                                })
                                ->addColumn('image2', function ($item) use ($url) {
                                    return '<img src='."$url".'/'."$item->upload_ktp".' width="100"> ';
                                })
                                ->addColumn('image3', function ($item) use ($url) {
                                    return '<img src='."$url".'/'."$item->upload_pembayaran".' width="100"> ';
                                })
                                ->addColumn('created_at', function ($item) {
                                    return date('d/m/Y', strtotime($item->created_at));
                                })
                                ->addColumn('delete', function ($val) {
                                    return "<button 
                                                data-item='".json_encode($val)."' onclick='hapus(this)'
                                                class='btn btn-xs btn-danger'><i class='fa fa-trash'></i>
                                            </button>";
                                })
                                ->addColumn('edit', function ($val) {
                                    return "<button
                                                data-title='Edit' data-toggle='modal' data-target='#edit'
                                                data-item='".json_encode($val)."' onclick='edit(this)'
                                                class='btn btn-xs btn-info'><i class='fa fa-check'></i>
                                            </button>";
                                })->rawColumns(['action','edit','delete'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $coin = UltraCoin::where('user_id', $request->id)->first();
        $coinstarter = DB::table('coins')->first();

        $user->update([
            'isActive'=> 1,
            'isRegistration' => 0,
        ]);

        $coin->update([
            'isActive' => 1,
            'coin' => $coinstarter->coin,
            'tgl_mining' => Carbon::now()->format('yy-m-d'),
        ]);

        Mail::to($user->email)->send(new VerifikasiUserMail($user));
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $user = User::findOrFail($id);

        $user->kodevoucher->update([
            'isActive'=> 1,
            'user_id' => NULL,
        ]);
        
        $user->ultracoindelete->delete();
        $user->userdetail->delete();
        $user->sponsor->delete();
        $user->regisrasiakun->delete();
        $user->bonussponspor->delete();

        $user->delete();

        return redirect()->back();
    }
}
