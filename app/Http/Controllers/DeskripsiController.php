<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Deskripsi;
use Illuminate\Http\Request;

class DeskripsiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $support = Deskripsi::where('keterangan', 'support')->first();
        $informasi = Deskripsi::where('keterangan', 'informasi')->first();

        return view('deskripsi.index', compact('support', 'informasi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateInformasi(Request $request, $id)
    {
        $deskripsi = Deskripsi::findOrFail($id);

        $deskripsi->update($request->all());

        return redirect()->back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexKirimChip()
    {
        $users = User::with('userdetail')->orderBy('created_at', 'desc')
            ->where('isActive', 1)->where('role', 'user')->get();

        return view('deskripsi.management.kirim-chip', compact('users'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexTukarWbzstockroom()
    {
        $users = User::with('userdetail')->orderBy('created_at', 'desc')
            ->where('isActive', 1)->where('role', 'user')->get();
            
        return view('deskripsi.management.tukar-wbz-stockroom');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
