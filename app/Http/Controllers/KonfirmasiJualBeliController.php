<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Auth;
use Session;
use App\User;
use Carbon\Carbon;
use App\Models\BeliCoin;
use Illuminate\Http\Request;
use App\Mail\EmailVerifikasiJualCoin;
use App\Mail\EmailPembatalanJualCoin;

class KonfirmasiJualBeliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = BeliCoin::orderBy('created_at', 'desc')->get();

        return view('konfirmasi-jual-beli-coin.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $uuid, $id)
    {
        $this->validate($request, [
            'coin'  => 'required|numeric',
            'biaya' => 'required'
        ]);

        $user = User::where('id', $uuid)->first();
        $coin = BeliCoin::with('user')->with('ultracoinpembeli')->with('ultracoinpenjual')->where('id', $id)->first();
        
        $now = Carbon::now()->format('Y-m-d');
        
        if (is_null($user)){
            Session::flash('warning', 'Mohon maaf ada kesalahan, dan data tidak dapat di inputkan!');
            return redirect()->back();
        } else {

            if ($coin->ultracoinpenjual->coin < $coin->coin){
                Session::flash('warning', 'Mohon maaf ada kesalahan, saldo chip anda kurang untuk melakukan penjualan!');
            } else {
                $noLastId = DB::table('beli_coins')->max('faktur');
                if (is_null($noLastId)){
                    $substr = 1;
                } else {
                    $substr = substr($noLastId, -1)+1;
                }
                $date   = Carbon::now()->format('Ymd');
                $time   = Carbon::now()->format('his');
                $faktur = $date.'/'.$time.'/'.$substr;
                $coin->update([
                    'faktur'    => $faktur,
                    'isActive'  => 1
                ]);

                if ($coin->ultracoinpembeli->tgl_mining == $now){
                    $coin->ultracoinpembeli->update([
                        'topup' => $coin->coin,
                        'upload_pembayaran' => $coin->upload_bukti
                    ]);
                } else {
                    $coin->ultracoinpembeli->create([
                        'user_id' => $coin->user_id_pembeli,
                        'tgl_mining' => $now,
                        'coin' => $coin->ultracoinpembeli->coin,
                        'isActive' => 1,
                        'topup' => $coin->coin,
                        'upload_pembayaran' => $coin->upload_bukti
                    ]);
                }

                $coin->ultracoinpenjual->update([
                    'coin' => ($coin->ultracoinpenjual->coin - $coin->coin),
                    'upload_pembayaran' => $coin->upload_bukti
                ]);

                Mail::to($coin->user->email)->send(new EmailVerifikasiJualCoin($coin));

                Session::flash('success', 'Berhasil, anda telah memberikan chip ke pada penerima dan otomatis chip anda berkurang.');
            }

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function batal(Request $request, $uuid, $id)
    {
        $user = BeliCoin::with('user')->where('id', $id)->first();
        
        $user->update([
            'isActive'  => 2
        ]);

        Mail::to($user->user->email)->send(new EmailPembatalanJualCoin($user));

        Session::flash('success', 'Berhasil, anda telah membatalkan penjualan chip.');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
