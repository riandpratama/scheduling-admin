<?php

namespace App\Http\Controllers\Store;

use Mail;
use Session;
use App\User;
use Carbon\Carbon;
use App\Models\UltraCoin;
use Illuminate\Http\Request;
use App\Models\WbzStockroom;
use App\Rules\MatchOldPassword;
use App\Models\WbzStockroomRiwayat;
use App\Http\Controllers\Controller;
use App\Mail\KonfirmasiKirimWbzStockroom;

class KirimSaldowbzMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('isActive', 1)->orderBy('username', 'asc')->where('username', '!=' ,'Administrator')->get();

        return view('store.kirim-saldo-member.index', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'wbz'  => 'required',
            'biaya' => 'required',
            'password' => ['required', new MatchOldPassword],
        ]);

        $userCheck = User::where('id', $request->user_id)->first();
        
        if (is_null($userCheck)){
            Session::flash('warning', 'Mohon maaf ada kesalahan, dan data tidak dapat di inputkan!');
            return redirect()->back();
        } else {
            $wbz = WbzStockroom::where('user_id', $request->user_id)->where('status', 1)->first();

            if (is_null($wbz)) {
                $wbzCreate = WbzStockroom::create([
                    'user_id' => $request->user_id,
                    'wbz' => $request->wbz,
                    'status' => 1,
                ]);
                $riwayat = WbzStockroomRiwayat::create([
                    'user_id' => $request->user_id,
                    'wbz_stockroom_id' => $wbzCreate->id,
                    'wbz' => $request->wbz,
                    'rupiah' => $request->biaya,
                    'status' => 1,
                    'keterangan' => 'kirim-saldo-member'
                ]);
            } else {
                
                $wbz->update([
                    'user_id' => $request->user_id,
                    'wbz' => $wbz->wbz + $request->wbz,
                    'status' => 1,
                ]);
                $riwayat = WbzStockroomRiwayat::create([
                    'user_id' => $request->user_id,
                    'wbz_stockroom_id' => $wbz->id,
                    'wbz' => $request->wbz,
                    'rupiah' => $request->biaya,
                    'status' => 1,
                    'keterangan' => 'kirim-saldo-member'
                ]);
            }

            // Mail::to(config('app.email'))->send(new EmailStoreTopupWbz($riwayat));
            // Mail::to($userCheck->email)->send(new EmailStoreTopupWbz($riwayat));

            Session::flash('success', 'Berhasil, mengirim chip WbzStockroom kepada member.');
            
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = WbzStockroomRiwayat::findOrFail($id);
        
        $data->delete();

        Session::flash('success', 'Berhasil, membatalkan.');

        return redirect()->back();
    }
}
