<?php

namespace App\Http\Controllers\Store;

use Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\UltraCoin;
use App\Models\WbzStockroom;
use App\Models\WbzStockroomRiwayat;
use App\Http\Controllers\Controller;

class RiwayatStoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function wbzstockroom()
    {
    	$data = WbzStockroomRiwayat::with('user')
    						->with('wbzstockroom')
    						->where('keterangan', 'tukar-saldo')
    						->where('status', 1)
    						->orderBy('created_at', 'desc')
    						->get();

        return view('store.riwayat.saldo-wbz', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function wbzchip()
    {
    	$data = WbzStockroomRiwayat::with('user')
    						->with('wbzstockroom')
    						->where('keterangan', 'ambil-saldo')
    						->where('status', 1)
    						->orderBy('created_at', 'desc')
    						->get();

        return view('store.riwayat.saldo-chip', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function kirimwbzstockroom()
    {
        $data = WbzStockroomRiwayat::with('user')
                            ->with('wbzstockroom')
                            ->where('keterangan', 'kirim-saldo')
                            ->where('status', 1)
                            ->orderBy('created_at', 'desc')
                            ->get();

        return view('store.riwayat.kirim-wbz', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function kirimMemberwbzstockroom()
    {
        $data = WbzStockroomRiwayat::with('user')
                            ->with('wbzstockroom')
                            ->where('keterangan', 'kirim-saldo-member')
                            ->where('status', 1)
                            ->orderBy('created_at', 'desc')
                            ->get();

        return view('store.riwayat.kirim-wbz-member', compact('data'));
    }
}