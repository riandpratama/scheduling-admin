<?php

namespace App\Http\Controllers\Store;

use Session;
use App\Models\Pemesanan;
use App\Models\WbzStockroom;
use Illuminate\Http\Request;
use App\Models\WbzStockroomRiwayat;
use App\Http\Controllers\Controller;

class PemesananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pemesanan = Pemesanan::orderBy('created_at', 'desc')->get();

        return view('store.pemesanan.index', compact('pemesanan'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pemesanan = Pemesanan::findOrFail($id);

        return view('store.pemesanan.show', compact('pemesanan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pemesanan = Pemesanan::findOrFail($id);

        $wbz = WbzStockroom::where('user_id', $pemesanan->user_id_pembeli)->first();

        if ($request->status == 1) {
            if (!is_null($wbz)){
                if ($wbz->wbz >= $pemesanan->harga_coin) {

                    $pemesanan->update([
                        'status' => $request->status,
                        'tgl_konfirmasi' => \Carbon\Carbon::now()->format('Y/m/d')
                    ]);

                    $wbz->update([
                        'wbz' => ($wbz->wbz - $pemesanan->harga_coin)
                    ]);

                    WbzStockroomRiwayat::create([
                        'user_id' => $wbz->user_id,
                        'wbz_stockroom_id' => $wbz->id,
                        'wbz' => $pemesanan->harga_coin,
                        'rupiah' => $pemesanan->harga_rupiah,
                        'status' => 1,
                        'keterangan' => 'pembelian'
                    ]);

                    Session::flash('success', 'Berhasil, Pemesanan telah di konfirmasi.');
                    return redirect()->route('pemesanan.index');
                } else {
                    Session::flash('warning', 'Gagal Saldo WbzStockroom tidak mencukupi!');
                    return redirect()->route('pemesanan.index');
                }
            } else {
                Session::flash('warning', 'Gagal Saldo WbzStockroom tidak di temukan!');
                return redirect()->route('pemesanan.index');
            }
        } else {
            $pemesanan->update([
                'status' => $request->status, 
                'tgl_konfirmasi' => \Carbon\Carbon::now()->format('Y/m/d')
            ]);
            
            Session::flash('success', 'Pemesanan telah ditolak.');
            return redirect()->route('pemesanan.index');
        }
    }
}
