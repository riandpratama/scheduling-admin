<?php

namespace App\Http\Controllers\Store;

use Auth;
use Image;
use Storage;
use File;
use Session;
use Carbon\Carbon;
use App\Models\Produk;
use App\Models\ProdukImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProdukController extends Controller
{
    public $path;
    public $paths;
    public $dimensions;

    public function __construct()
    {
        //DEFINISIKAN PATH
        $this->path = storage_path('app/public/images');
        $this->paths = storage_path('app/public/images/detail');
        //DEFINISIKAN DIMENSI
        $this->dimensions = ['500'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::orderBy('created_at', 'desc')->get();

        return view('store.produk.index', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('store.produk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!File::isDirectory($this->path)) {
            //MAKA FOLDER TERSEBUT AKAN DIBUAT
            File::makeDirectory($this->path);
        }
        
        //MENGAMBIL FILE IMAGE DARI FORM
        $file = $request->file('image');
        //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
        //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
        Image::make($file)->save($this->path . '/' . $fileName);
        
        //LOOPING ARRAY DIMENSI YANG DI-INGINKAN
        //YANG TELAH DIDEFINISIKAN PADA CONSTRUCTOR
        foreach ($this->dimensions as $row) {
            //MEMBUAT CANVAS IMAGE SEBESAR DIMENSI YANG ADA DI DALAM ARRAY 
            $canvas = Image::canvas($row, $row);
            //RESIZE IMAGE SESUAI DIMENSI YANG ADA DIDALAM ARRAY 
            //DENGAN MEMPERTAHANKAN RATIO
            $resizeImage  = Image::make($file)->resize($row, $row, function($constraint) {
                $constraint->aspectRatio();
            });
            
            //CEK JIKA FOLDERNYA BELUM ADA
            if (!File::isDirectory($this->path . '/' . $row)) {
                //MAKA BUAT FOLDER DENGAN NAMA DIMENSI
                File::makeDirectory($this->path . '/' . $row);
            }
            
            //MEMASUKAN IMAGE YANG TELAH DIRESIZE KE DALAM CANVAS
            $canvas->insert($resizeImage, 'center');
            //SIMPAN IMAGE KE DALAM MASING-MASING FOLDER (DIMENSI)
            $canvas->save($this->path . '/' . $row . '/' . $fileName);
        }

        $produk = Produk::create([
            'user_id' => Auth::user()->id,
            'reference' => $request->reference,
            'nama_produk' => $request->nama_produk,
            'kategori' => $request->kategori,
            'deskripsi' => $request->deskripsi,
            'harga_coin' => $request->harga_coin,
            'harga_rupiah' => str_replace(',', '', str_replace('.', '',$request->harga_rupiah)) ?: 0,
            'stok' => $request->stok,
            'image_sampul' => $fileName
        ]);

        $files = $request->file('image_detail');

        if(!empty($files)):
            foreach($files as $file):

                if (!File::isDirectory($this->paths)) {
                    File::makeDirectory($this->paths);
                }
                
                $fileNameDetail = Carbon::now()->timestamp . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                Image::make($file)->save($this->paths . '/' . $fileNameDetail);

                foreach ($this->dimensions as $row) {
                    $canvas = Image::canvas($row, $row);
                    $resizeImage  = Image::make($file)->resize($row, $row, function($constraint) {
                        $constraint->aspectRatio();
                    });
                    
                    if (!File::isDirectory($this->paths . '/' . $row)) {
                        File::makeDirectory($this->paths . '/' . $row);
                    }
                    
                    $canvas->insert($resizeImage, 'center');
                    $canvas->save($this->paths . '/' . $row . '/' . $fileNameDetail);
                }

                ProdukImage::create([
                    'produk_id' => $produk->id,
                    'image_path' => $fileNameDetail
                ]);

            endforeach;
        endif;

        return redirect()->route('produk.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::findOrFail($id);

        return view('store.produk.edit', compact('produk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detailImage($id)
    {
        $produk = Produk::findOrFail($id);
        
        return view('store.produk.detail', compact('produk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeDetailProduk(Request $request, $id)
    {
        $files = $request->file('image_detail');

        if(!empty($files)):
            foreach($files as $file):

                if (!File::isDirectory($this->paths)) {
                    File::makeDirectory($this->paths);
                }
                
                $fileNameDetail = Carbon::now()->timestamp . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                Image::make($file)->save($this->paths . '/' . $fileNameDetail);

                foreach ($this->dimensions as $row) {
                    $canvas = Image::canvas($row, $row);
                    $resizeImage  = Image::make($file)->resize($row, $row, function($constraint) {
                        $constraint->aspectRatio();
                    });
                    
                    if (!File::isDirectory($this->paths . '/' . $row)) {
                        File::makeDirectory($this->paths . '/' . $row);
                    }
                    
                    $canvas->insert($resizeImage, 'center');
                    $canvas->save($this->paths . '/' . $row . '/' . $fileNameDetail);
                }

                ProdukImage::create([
                    'produk_id' => $id,
                    'image_path' => $fileNameDetail
                ]);

            endforeach;
        endif;

        return redirect()->back();
    }

    public function destroyDetailProduk($id)
    {
        $data = ProdukImage::findOrFail($id);
        
        Storage::delete(asset('storage/images/detail/500/'.$data->image_path));
        Storage::delete(asset('storage/images/detail/'.$data->image_path));
        $data->delete();

        Session::flash('success', 'The Image has successfully deleted.');

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Produk::findOrFail($id);

        if (!File::isDirectory($this->path)) {
            File::makeDirectory($this->path);
        }
        
        if ($request->file('image') !== null && $request->file('image') !== ''){
            Storage::delete(asset('storage/images/detail/'.$data->image_sampul));
        }
        
        if ($request->hasFile('image')) {
        
            $file = $request->file('image');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
            Image::make($file)->save($this->path . '/' . $fileName);
            foreach ($this->dimensions as $row) {
                $canvas = Image::canvas($row, $row);
                $resizeImage  = Image::make($file)->resize($row, $row, function($constraint) {
                    $constraint->aspectRatio();
                });
                
                if (!File::isDirectory($this->path . '/' . $row)) {
                    File::makeDirectory($this->path . '/' . $row);
                }
                
                $canvas->insert($resizeImage, 'center');
                $canvas->save($this->path . '/' . $row . '/' . $fileName);
            }

        } else {
            $fileName = $data->image_sampul;
        }

        $data->update([
            'reference' => $request->reference,
            'nama_produk' => $request->nama_produk,
            'kategori' => $request->kategori,
            'deskripsi' => $request->deskripsi,
            'harga_coin' => $request->harga_coin,
            'harga_rupiah' => str_replace(',', '', str_replace('.', '',$request->harga_rupiah)) ?: 0,
            'stok' => $request->stok,
            'image_sampul' => $fileName
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Produk::findOrFail($id);

        Storage::delete(asset('storage/images/detail/'.$data->image_path));
        $data->produkImage()->delete();
        $data->delete();

        Session::flash('success', 'The Image has successfully deleted.');

        return redirect()->back();
    }
}
