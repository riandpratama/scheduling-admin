<?php

namespace App\Http\Controllers\Store;

use Mail;
use Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\UltraCoin;
use App\Models\WbzStockroom;
use App\Models\WbzStockroomRiwayat;
use App\Http\Controllers\Controller;
use App\Mail\KonfirmasiKirimWbzStockroom;

class KirimSaldowbzController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = WbzStockroomRiwayat::with('user')->with('wbzstockroom')->where('keterangan', 'kirim-saldo')->where('status', 0)->orderBy('created_at', 'desc')->get();

        return view('store.kirim-saldo.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $data = WbzStockroomRiwayat::findOrFail($id);
        
        $data->update(['status' => 1]);

        if ($data->wbzstockroom->status == 1) {
            $data->wbzstockroom->update([
                'status' => 1,
                'wbz' => ($data->wbzstockroom->wbz - $data->wbz)
            ]);
        } else {
            $data->wbzstockroom->update([
                'status' => 1
            ]);
        }

        Mail::to($data->user->email)->send(new KonfirmasiKirimWbzStockroom($data));
        
        Session::flash('success', 'Berhasil, memverifikasi.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = WbzStockroomRiwayat::findOrFail($id);
        
        $data->delete();

        Session::flash('success', 'Berhasil, membatalkan.');

        return redirect()->back();
    }
}
