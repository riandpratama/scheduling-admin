<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Session;
use Carbon\Carbon;
use App\Models\AmbilCoin;
use App\Models\UltraCoin;
use Illuminate\Http\Request;
use App\Mail\VerifikasiAmbilCoin;

class KonfirmasiAmbilCoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = AmbilCoin::with('user')->with('userdetail')->where('isActive', 1)->orderBy('created_at', 'desc')->get();

        return view('konfirmasi-ambil-coin.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $userCheck = AmbilCoin::with('user')->where('id', $id)->first();
        $user = AmbilCoin::with('user')->where('id', $id)->first();
        
        if (is_null($userCheck)){
            Session::flash('warning', 'Mohon maaf ada kesalahan, dan data tidak dapat di inputkan!');
            return redirect()->back();
        } else {

            $coin = UltraCoin::where('user_id', $request->user_id)->orderBy('tgl_mining', 'desc')->first();
            
            $ambilCoin = AmbilCoin::with('user')->with('ultracoin')->findOrFail($id);
            
            $noLastId = DB::table('ambil_coins')->max('faktur');
            
            if (is_null($noLastId)){
                $substr = 1;
            } else {
                $substr = substr($noLastId, -1)+1;
            }

            $date   = Carbon::now()->format('Ymd');
            $time   = Carbon::now()->format('his');
            $faktur = $date.'/'.$time.'/'.$substr;

            $ambilCoin->update(['isActive' => 2, 'faktur' => $faktur, 'ultra_coin_id' => $ambilCoin->ultracoin->id]);

            $coin->update(['coin' => ($coin->coin - $request->coin_diambil)]);

            Mail::to($user->user->email)->send(new VerifikasiAmbilCoin($user));

            Session::flash('success', 'Berhasil, memverifikasi.');

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = AmbilCoin::with('user')->where('id', $id)->first();

        $data->delete();

        Session::flash('success', 'Berhasil, menghapus.');
            
        return redirect()->back();
    }
}
