<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Models\AmbilCoin;
use App\Models\KirimCoin;
use App\Models\BonusSponsor;
use App\Models\KonfirmasiTopup;
use Illuminate\Http\Request;

class RiwayatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function topup()
    {
        $data = KonfirmasiTopup::with('user')->where('isActive',3)->orderBy('created_at', 'desc')->get();

        return view('riwayat.topup', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jualChip()
    {
        $data = AmbilCoin::where('isActive',2)->orderBy('created_at', 'desc')->get();

        return view('riwayat.jual-chip', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function kirimChip()
    {
        $data = KirimCoin::orderBy('created_at', 'desc')->get();

        return view('riwayat.kirim-chip', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bonusSponsor()
    {
        $data = BonusSponsor::orderBy('created_at', 'desc')->get();

        return view('riwayat.bonus-sponsor', compact('data'));
    }
}
