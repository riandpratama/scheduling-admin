<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['users'] = DB::table('users')->where('isActive', 1)->where('username', '!=', 'Administrator')->get()->count();
        $data['vouchers_aktif'] = DB::table('kode_vouchers')->where('isActive', 1)->get()->count();
        $data['vouchers_tidak_aktif'] = DB::table('kode_vouchers')->where('isActive', 0)->get()->count();
        $data['konfirmasi_topup'] = DB::table('konfirmasi_topups')->where('isActive',2)->get()->count();
        $data['jual_chip'] = DB::table('ambil_coins')->where('isActive', 1)->get()->count();
        $data['produk'] = DB::table('toko_produks')->get()->count();
        $data['pemesanan'] = DB::table('toko_pemesanans')->get()->count();
        $data['tukar_wbzstockroom'] = DB::table('wbz_stockroom_riwayats')->where('keterangan', 'tukar-saldo')->where('status', 0)->get()->count();
        $data['tukar_chip_wbz'] = DB::table('wbz_stockroom_riwayats')->where('keterangan', 'ambil-saldo')->where('status', 0)->get()->count();

        return view('home', $data);
    }
}
