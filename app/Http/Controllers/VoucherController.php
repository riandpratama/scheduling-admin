<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use App\User;
use DataTables;
use Carbon\Carbon;
use App\Models\KodeVoucher;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\VerifikasiUserMail;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $user = DB::table('users')->select('id', 'username', 'email')->orderBy('created_at', 'desc')->where('isActive', 1)->get();

        $voucher = KodeVoucher::with('userPemberi')->with('userPenerima')->orderBy('created_at', 'desc')->get();
        
        return view('voucher.index', compact('user', 'voucher'));
    }

    /**
     * Create a new data
     * array $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        KodeVoucher::create([
            'user_id_pemberi' => $request->user_id_pemberi,
            'kode_vouchers' => \Str::random(8),
            'isActive' => 1,
            'biaya' => 400000
        ]);

        return redirect()->back();
    }

    public function destroy($id)
    {
        $data = KodeVoucher::findOrFail($id);

        $data->delete();
        
        return redirect()->back();
    }
}