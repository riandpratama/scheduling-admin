<?php

namespace App\Http\Controllers;

use Mail;
use Session;
use Carbon\Carbon;
use App\Models\KonfirmasiTopup;
use Illuminate\Http\Request;
use App\Mail\VerifikasiTopup;

class KonfirmasiTopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = KonfirmasiTopup::with('user')->where('isActive',2)->orderBy('created_at', 'desc')->get();

        return view('konfirmasi-topup.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $user = KonfirmasiTopup::with('user')->with('ultracoin')->where('id', $id)->first();
        $now = Carbon::now()->format('Y-m-d');

        if (is_null($user)){
            Session::flash('warning', 'Mohon maaf ada kesalahan, dan data tidak dapat di inputkan!');
            return redirect()->back();
        } else {
            if ($user->ultracoin->tgl_mining == $now){
                $user->ultracoin->update([
                    'topup' => $request->coin,
                    'upload_pembayaran' => $user->upload_bukti
                ]);

                $user->update([
                    'isActive' => 3,
                    'ultra_coin_id' => $user->ultracoin->id
                ]);
            } else {
                $id = $user->ultracoin->create([
                    'user_id' => $user->id,
                    'tgl_mining' => $now,
                    'coin' => $user->ultracoin->coin,
                    'isActive' => 1,
                    'topup' => $request->coin,
                    'upload_pembayaran' => $user->upload_bukti
                ]);

                $user->update([
                    'isActive' => 3,
                    'ultra_coin_id' => $id->id
                ]);
            }

            Mail::to($user->user->email)->send(new VerifikasiTopup($user));

            Session::flash('success', 'Berhasil, memverifikasi.');
            
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = KonfirmasiTopup::with('user')->with('ultracoin')->where('id', $id)->first();

        $data->delete();

        Session::flash('success', 'Berhasil, menghapus.');
            
        return redirect()->back();
    }
}
