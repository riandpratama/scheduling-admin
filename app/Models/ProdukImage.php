<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdukImage extends Model
{
    protected $table = 'toko_produk_images';
    
    protected $guarded = [];
}
