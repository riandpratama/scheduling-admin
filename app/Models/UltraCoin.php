<?php

namespace App\Models;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class UltraCoin extends Model
{
    protected $table = 'ultra_coins';
    
    protected $guarded = [];

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }
    
    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function topupSaldo()
    {
        return $this->hasOne('App\Models\KonfirmasiTopup', 'ultra_coin_id', 'id');
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function jualSaldo()
    {
        return $this->hasOne('App\Models\AmbilCoin', 'ultra_coin_id', 'id');
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function kirimSaldo()
    {
        return $this->hasOne('App\Models\KirimCoin', 'ultra_coin_id', 'id');
    }
}
