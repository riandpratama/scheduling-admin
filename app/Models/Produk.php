<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Produk extends Model
{
	use Sluggable;

    protected $table = 'toko_produks';
    
    protected $guarded = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nama_produk'
            ]
        ];
    }

    public function setReferenceAttribute()
    {
        return $this->attributes['reference'] = '#'.str_random(10);
    }

    public function produkImage()
    {
        return $this->hasMany(ProdukImage::class, 'produk_id');
    }
}
