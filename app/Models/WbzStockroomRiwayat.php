<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WbzStockroomRiwayat extends Model
{
    protected $table = 'wbz_stockroom_riwayats';
    
    protected $guarded = [];

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function wbzstockroom()
    {
        return $this->belongsTo('App\Models\WbzStockroom', 'wbz_stockroom_id');
    }
}
