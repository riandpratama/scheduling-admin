<?php

namespace App\Models;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class Pemesanan extends Model
{
    protected $table = 'toko_pemesanans';
    
    protected $guarded = [];

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function pembeli()
    {
        return $this->belongsTo('App\User', 'user_id_pembeli');
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function penjual()
    {
        return $this->belongsTo('App\User', 'user_id_penjual');
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function pemesanandetail()
    {
        return $this->hasMany(PemesananDetail::class, 'pemesanan_id');
    }
}