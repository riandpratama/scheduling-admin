<?php

namespace App\Models;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class KonfirmasiTopup extends Model
{
    protected $table = 'konfirmasi_topups';
    
    protected $guarded = [];

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }
    
    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    /**
     *  Setup model user
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     *  Setup model ultracoin
     */
    public function ultracoin()
    {
        return $this->hasOne('App\Models\UltraCoin', 'user_id', 'user_id')->orderBy('created_at', 'desc');
    }
}
