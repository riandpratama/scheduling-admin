<?php

namespace App\Models;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class PemesananDetail extends Model
{
    protected $table = 'toko_pemesanan_details';
    
    protected $guarded = [];

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function produk()
    {
        return $this->belongsTo('App\Models\Produk', 'produk_id');
    }
}