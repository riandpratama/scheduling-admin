<?php

namespace App\Models;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class AmbilCoin extends Model
{
    protected $table = 'ambil_coins';
    
    protected $guarded = [];

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }
    
    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    /**
     *  Setup model user
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     *  Setup model user
     */
    public function userdetail()
    {
        return $this->hasOne('App\Models\UserDetail', 'user_id', 'user_id');
    }

    // /**
    //  *  Setup model ultracoin
    //  */
    // public function ultracoin()
    // {
    //     return $this->hasOne('App\Models\UltraCoin', 'ultra_coin_id')->orderBy('created_at', 'desc')->first();
    // }

    /**
     *  Setup model ultracoin
     */
    public function ultracoin()
    {
        return $this->hasOne('App\Models\UltraCoin', 'user_id', 'user_id')->orderBy('created_at', 'desc');
    }
}
