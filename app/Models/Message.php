<?php

namespace App\Models;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'toko_messages';
    
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function userfrom()
    {
    	return $this->belongsTo('App\User', 'from')->withDefault();
    }

    public function userto()
    {
    	return $this->belongsTo('App\User', 'to');
    }
}