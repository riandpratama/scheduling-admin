<?php

namespace App\Models;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class KodeVoucher extends Model
{
    protected $table = 'kode_vouchers';
    
    protected $guarded = [];

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }
    
    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }

    public function userPemberi()
    {
        return $this->belongsTo('App\User','user_id_pemberi', 'id');
    }

    public function userPenerima()
    {
        return $this->belongsTo('App\User','user_id')->withDefault();
    }

    public function sponsor()
    {
        return $this->hasOne('App\Models\Sponsor','user_id', 'user_id_pemberi');
    }
}
