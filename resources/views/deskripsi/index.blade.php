@extends('adminlte::page')

@section('title', 'Ubah Informasi')

@section('content')

	@if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    @if (Session::has('warning'))
        <div class="alert alert-warning">
            {{ Session::get('warning') }}
        </div>
    @endif

    <div class="col-md-12">
        <div class="row">
        	<div class="col-md-3">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h5 class="box-title">Support</h5>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">
                    	<button class="pull-right" data-title="support" data-toggle="modal" data-target="#support" ><i class="fa fa-edit"></i></button>
                        <div>
                            {!! $support->konten !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h5 class="box-title">Informasi</h5>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">
                    	<button class="pull-right" data-title="informasi" data-toggle="modal" data-target="#informasi" ><i class="fa fa-edit"></i></button>
                        <div>
                            {!! $informasi->konten !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="support" tabindex="-1" role="dialog" aria-labelledby="support" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
	          <div class="modal-header">
	          	<h4>Edit Support</h4>
		        <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
		      </div>
		          <div class="modal-body">
		          	<form id="formSupport" method="POST" action="{{ route('deskripsi.update-informasi', 1) }}">
		          		{{ csrf_field() }}
		                <textarea class="form-control" name="konten" id="mytextarea" cols="30" rows="10">
		                	{{ $support->konten }}
		                </textarea>
		                <br>
		                <button type="submit" class="btn btn-success" onclick="return confirm('Anda yakin ingin merubah?')">
		                	<span class="glyphicon glyphicon-check" aria-hidden="true"></span> Simpan
		                </button>

		        		<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		            </form>
		        </div>
		    </div>
		</div>
	</div>

	<div class="modal fade" id="informasi" tabindex="-1" role="dialog" aria-labelledby="informasi" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
	          <div class="modal-header">
	          	<h4>Edit Informasi</h4>
		        <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
		      </div>
		          <div class="modal-body">
		          	<form id="formSupport" method="POST" action="{{ route('deskripsi.update-informasi', 2) }}">
		          		{{ csrf_field() }}
		                <textarea class="form-control" name="konten" id="mytextarea-informasi" cols="30" rows="10">
		                	{{ $informasi->konten }}
		                </textarea>
		                <br>
		                <button type="submit" class="btn btn-success" onclick="return confirm('Anda yakin ingin merubah?')">
		                	<span class="glyphicon glyphicon-check" aria-hidden="true"></span> Simpan
		                </button>

		        		<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		            </form>
		        </div>
		    </div>
		</div>
	</div>



    <div class="col-lg-3 col-xs-6">
        <a href="{{ route('deskripsi.index.kirim-chip') }}">
            <div class="box inner">
                <figure class="figure">
                    <div class="figure-img">
                        <div class="small-box bg-green">
                            <div class="inner">
                                <br>
                                <h3><i class="ion ion-ios-cloud-download-outline"></i></h3>
                            </div>
                            <a href="{{ route('deskripsi.index.kirim-chip') }}" class="small-box-footer"><b>Kirim Chip</b></a>
                        </div>
                    </div>
                </figure>
            </div>
        </a>
    </div>

    <div class="col-lg-3 col-xs-6">
        <a href="{{ route('deskripsi.index.tukar-wbz-stockroom') }}">
            <div class="box inner">
                <figure class="figure">
                    <div class="figure-img">
                        <div class="small-box bg-green">
                            <div class="inner">
                                <br>
                                <h3><i class="fas fa-fw fa-clone "></i></h3>
                            </div>
                            <a href="{{ route('deskripsi.index.tukar-wbz-stockroom') }}" class="small-box-footer"><b>Tukar wbz stockroom</b></a>
                        </div>
                    </div>
                </figure>
            </div>
        </a>
    </div>

	<script src="https://cdn.tiny.cloud/1/w6vzdd6iabbywgbtaez9oijzj2jymhc500w1obh9ftcvxlkd/tinymce/5/tinymce.min.js" referrerpolicy="origin"/></script>
	<script>
      tinymce.init({
        selector: '#mytextarea'
      });
    </script>
    <script>
      tinymce.init({
        selector: '#mytextarea-informasi'
      });
    </script>
@endsection