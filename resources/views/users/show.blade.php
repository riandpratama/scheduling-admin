@extends('adminlte::page')

@section('title', 'Pendaftar')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
            	<div class="box-header with-border">
	              	<h3 class="box-title">Data User</h3>
		            <div class="box-tools pull-right">
		                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                </button>
		            </div>
	            </div>
	            
		        <div class="box-body">
	            	<div class="col-sm-12 table-responsive">
		                <table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
			                            <center>Hari</center>
			                        </th>
			                        <th>
			                            <center>Tanggal</center>
			                        </th>
			                        <th>
			                            <center>Chip</center>
			                        </th>
			                        <th>
			                            <center>Topup</center>
			                        </th>
			                        <th>
			                            <center>JualChip</center>
			                        </th>
			                        <th>
			                            <center>KirimChip</center>
			                        </th>
			                        <th>
			                            <center>TerimaChip</center>
			                        </th>
			                    </tr>
			                </thead>
			                <tbody>
			                	@foreach($user->ultracoin as $item)
			                	<tr>
			                		<td><center>{{ $loop->iteration }}</center></td>
			                		<td><center>{{ date('d/m/Y', strtotime($item->tgl_mining)) }}</center></td>
			                		<td><center>{{ $item->coin }}</center></td>
			                		<td><center>{{ ($item->topupSaldo != null) ? $item->topupSaldo['coin'] : '-' }}</center></td>
			                		<td><center>{{ ($item->jualSaldo != null) ? $item->jualSaldo['coin_diambil'] : '-' }}</center></td>
			                		<td><center>{{ ($item->kirimSaldo != null) ? $item->kirimSaldo['coin'] : '-' }}</center></td>
			                		<td><center>{{ ($item->topup_temporary != 0) ? $item['topup_temporary'] : '-' }}</center></td>
			                	</tr>
			                	@endforeach
			                </tbody>
	            		</table>
	            	</div>
		        </div>
	    	</div>
        </div>
    </div>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
	<script type="text/javascript">
	    $(function () {
	        $('#datatables').DataTable({
	        	"order": [[ 0, "desc" ]]
	        });
	    });
	</script>

	<script>
		$(document).ready(function() {
		    $('.js-select2').select2();
		});
	</script>
@stop