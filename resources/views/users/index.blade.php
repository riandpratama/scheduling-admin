@extends('adminlte::page')

@section('title', 'Pendaftar')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
            	<div class="box-header with-border">
	              	<h3 class="box-title">Data User</h3>
		            <div class="box-tools pull-right">
		                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                </button>
		            </div>
	            </div>
	            
		        <div class="box-body">
	            	<div class="col-sm-12 table-responsive">
		                <table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
			                            <center>No.</center>
			                        </th>
			                        <th>
			                            <center>Email</center>
			                        </th>
			                        <th>
			                            <center>Kode Sponsor</center>
			                        </th>
			                        <th>
			                            <center>Nama Lengkap</center>
			                        </th>
			                        <th>
			                            <center>Tanggal Daftar</center>
			                        </th>
			                        <th>
			                            <center>Detail</center>
			                        </th>
			                    </tr>
			                </thead>
			                <tbody>
			                	@foreach($users as $item)
			                	<tr>
			                		<td><center>{{ $loop->iteration }}.</center></td>
			                		<td><center>{{ $item->email }}</center></td>
			                		<td><center>{{ $item->nama_sponsor }}</center></td>
			                		<td><center>{{ $item->userdetail->nama_lengkap }}</center></td>
			                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
			                		<td>
			                			<a href="{{ route('user.show',$item->id) }}" class='btn btn-xs btn-info'><i class='fa fa-eye'></i></a>
                                    </td>
			                	</tr>
			                	@endforeach
			                </tbody>
	            		</table>
	            	</div>
		        </div>
	    	</div>
        </div>
    </div>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
	<script type="text/javascript">
	    $(function () {
	        $('#datatables').DataTable();
	    });
	</script>

	<script>
		$(document).ready(function() {
		    $('.js-select2').select2();
		});
	</script>
@stop