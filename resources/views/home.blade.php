@extends('adminlte::page')

@section('title', 'Wallbroz District')

@section('content')
	<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          	<div class="info-box bg-aqua">
            	<span class="info-box-icon"><i class="fa fa-users"></i></span>

            	<div class="info-box-content">
              		<span class="info-box-text">Total Member</span>
              		<span class="info-box-number">&nbsp;{{ $users }} Members</span>

              		<div class="progress">
                		<div class="progress-bar" style="width: 70%"></div>
              		</div>
                  	<span class="progress-description">
                    	<a href="{{ route('user') }}" class="btn btn-primary btn-xs"> <i class="fa fa-eye"></i> &nbsp;View All</a>
                  	</span>
            	</div>
          	</div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          	<div class="info-box bg-green">
            	<span class="info-box-icon"><i class="fa fa-gift"></i></span>

            	<div class="info-box-content">
              		<span class="info-box-text">Total Voucher</span>
              		<span class="info-box-number">{{ $vouchers_aktif }} Aktif / {{ $vouchers_tidak_aktif }} Tidak Aktif </span>

	              	<div class="progress">
	                	<div class="progress-bar" style="width: 70%"></div>
	              	</div>
	                <span class="progress-description">
	                    <a href="{{ route('voucher') }}" class="btn btn-success btn-xs"> <i class="fa fa-eye"></i> &nbsp;View All</a>
	                </span>
            	</div>
          	</div>
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          	<div class="info-box bg-yellow">
            	<span class="info-box-icon"><i class="fa fa-check"></i></span>

            	<div class="info-box-content">
              		<span class="info-box-text">Total Konfirmasi Topup</span>
              		<span class="info-box-number">{{ $konfirmasi_topup }} Data</span>

              		<div class="progress">
                		<div class="progress-bar" style="width: 70%"></div>
              		</div>
                  	<span class="progress-description">
                    	<a href="{{ route('konfirmasi-topup') }}" class="btn btn-warning btn-xs"> <i class="fa fa-eye"></i> &nbsp;View All</a>
                  	</span>
            	</div>
          	</div>
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          	<div class="info-box bg-red">
            	<span class="info-box-icon"><i class="fa fa-share"></i></span>

            	<div class="info-box-content">
              		<span class="info-box-text">Total konfirmasi Jual Chip Wbz</span>
              		<span class="info-box-number">{{ $jual_chip }} Data</span>

              		<div class="progress">
                		<div class="progress-bar" style="width: 70%"></div>
              		</div>
                  	<span class="progress-description">
                    	<a href="{{ route('konfirmasi-ambil-coin') }}" class="btn btn-danger btn-xs"> <i class="fa fa-eye"></i> &nbsp;View All</a>
                  	</span>
            	</div>
          	</div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          	<div class="info-box bg-aqua">
            	<span class="info-box-icon"><i class="fa fa-database"></i></span>

            	<div class="info-box-content">
              		<span class="info-box-text">Total Data Prduk / Jasa</span>
              		<span class="info-box-number">{{ $produk }} Data</span>

              		<div class="progress">
                		<div class="progress-bar" style="width: 70%"></div>
              		</div>
                  	<span class="progress-description">
                    	<a href="{{ route('produk.index') }}" class="btn btn-info btn-xs"> <i class="fa fa-eye"></i> &nbsp;View All</a>
                  	</span>
            	</div>
          	</div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          	<div class="info-box bg-green">
            	<span class="info-box-icon"><i class="fa fa-list"></i></span>

            	<div class="info-box-content">
              		<span class="info-box-text">Total Pemesanan</span>
              		<span class="info-box-number">{{ $pemesanan }} Pemesan</span>

	              	<div class="progress">
	                	<div class="progress-bar" style="width: 70%"></div>
	              	</div>
	                <span class="progress-description">
	                    <a href="{{ route('pemesanan.index') }}" class="btn btn-success btn-xs"> <i class="fa fa-eye"></i> &nbsp;View All</a>
	                </span>
            	</div>
          	</div>
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          	<div class="info-box bg-yellow">
            	<span class="info-box-icon"><i class="fa fa-clone"></i></span>

            	<div class="info-box-content">
              		<span class="info-box-text">Total Konfirmasi Wbzstockroom</span>
              		<span class="info-box-number">{{ $tukar_wbzstockroom }} Data</span>

              		<div class="progress">
                		<div class="progress-bar" style="width: 70%"></div>
              		</div>
                  	<span class="progress-description">
                    	<a href="{{ route('saldo-wbz.index') }}" class="btn btn-warning btn-xs"> <i class="fa fa-eye"></i> &nbsp;View All</a>
                  	</span>
            	</div>
          	</div>
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12">
          	<div class="info-box bg-red">
            	<span class="info-box-icon"><i class="fa fa-cube"></i></span>

            	<div class="info-box-content">
              		<span class="info-box-text">Total Konfirmasi Chip Wbz</span>
              		<span class="info-box-number">{{ $tukar_chip_wbz }} Data</span>

              		<div class="progress">
                		<div class="progress-bar" style="width: 70%"></div>
              		</div>
                  	<span class="progress-description">
                    	<a href="{{ route('saldo-wbz-chip.index') }}" class="btn btn-danger btn-xs"> <i class="fa fa-eye"></i> &nbsp;View All</a>
                  	</span>
            	</div>
          	</div>
        </div>
    </div>
@stop