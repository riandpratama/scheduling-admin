@extends('adminlte::page')

@section('title', 'Kirim WbzStockroom ke Chip WBZ')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.12/css/bootstrap-select.css" />

@section('content')

	<div class="box box-success">
		

        <form role="form" action="{{ route('kirim-wbz-member.store') }}" method="POST">
        	@csrf
	        <div class="box-body">
	        	@if (Session::has('success'))
		            <div class="alert alert-success">
		                {{ Session::get('success') }}
		            </div>
		        @endif
		        @if (Session::has('warning'))
		            <div class="alert alert-warning">
		                {{ Session::get('warning') }}
		            </div>
		        @endif
		        @foreach ($errors->all() as $error)
                    <p class="text-danger">{{ $error }}</p>
                 @endforeach

                 <div class="form-group">
                	<label for="idr_pengambilan">Pilih Member (username) <i style="color:red">*</i></label>
                    <select name="user_id" id="user" required="" class="selectpicker form-control show-tick" data-live-search="true" >
                    	<option disabled="" selected="" style="color: black;">Pilih Member</option>
                        @foreach($user as $item)
                        <option value="{{ $item->id }}">{{ $item->username }}</option>
                        @endforeach
                    </select>
                </div>
	            <div class="form-group">
	              	<label for="idr_pengambilan">Jumlah IDR Pengiriman <i style="color:red">*</i></label>
	              	<div class="input-group">
	                	<span class="input-group-addon">Rp</span>
	                	<input type="text" class="form-control idr" name="biaya" autocomplete="off">
              		</div>
	            </div>
	            <div class="form-group">
	              	<label for="wbz">Chip WbzStockroom </label>
	                <input type="number" class="form-control wbz" name="wbz" readonly="">
	            </div>
	            <div class="form-group">
	              	<label for="password">Password <i style="color:red">*</i></label>
	              	<input type="password" class="form-control" name="password" id="password" >
	            </div>
	        </div>

	        <div class="box-footer">
	            <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin submit?')"><i class="fa fa-save"></i> Submit</button>
	       	</div>
        </form>
        
     </div>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.12/js/bootstrap-select.js"></script>
<script type="text/javascript">
	$(".idr").keyup(function(){
	    var idr = $(".idr").val();
	    var jumlah = (parseInt(idr) / 20000);
	    $(".wbz").val(jumlah);
	});
</script>

@endsection