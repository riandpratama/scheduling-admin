@extends('adminlte::page')

@section('title', 'Riwayat Kirim Saldo Wbzstockrom')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h3 class="box-title">Riwayat Kirim Saldo Wbzstockrom (Member)</h3>

		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        <div class="box-body" style="">
	            	<div class="col-md-12 table-responsive">
	            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
		                <thead>
		                    <tr>
		                        <th>
	                            	<center>No.</center>
		                        </th>
		                        <th>
		                            <center>Tanggal</center>
		                        </th>
		                        <th>
		                            <center>Email</center>
		                        </th>
		                        <th>
		                            <center>Wbz</center>
		                        </th>
		                        <th>
		                            <center>Rupiah</center>
		                        </th>
		                    </tr>
		                </thead>
		                <tbody>
		                	@foreach($data as $item)
		                	<tr>
		                		<td><center>{{ $loop->iteration }}</center></td>
		                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
		                		<td><center>{{ $item->user->email }}</center></td>
		                		<td><center>{{ number_format($item->wbz, 2) }}</center></td>
		                		<td><center>Rp.{{ number_format($item->rupiah, 0, ',', '.') }}</center></td>
		                	</tr>
		                	@endforeach
		                </tbody>
		            	</table>
	          		</div>
		      	</div>
		    </div>
		</div>
	</div>
@endsection

@section('js')

<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

@endsection