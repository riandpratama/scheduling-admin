@extends('adminlte::page')

@section('title', 'Show Message')

@section('content')
    <div class="col-md-8">
        <div class="row">
            <div class="box box-success direct-chat direct-chat-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Chat Wallbroz</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>

                <div class="box-body" style="height:400px;">
                    <div class="direct-chat-messages" style="height: 500px;">
                        
                        @foreach($messages as $message)
                            <div class="direct-chat-msg {{ ($message->from == Auth::id()) ? 'right' : '' }}">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-{{ ($message->from == Auth::id()) ? 'left' : 'right' }}">
                                        {{ ($message->from == Auth::id()) ? $message->userfrom['username'] : $message->userfrom['username'] }}
                                    </span>
                                    <span class="direct-chat-timestamp pull-{{ ($message->from == Auth::id()) ? 'right' : 'left' }}">
                                        {{ date('d M y, h:i a', strtotime($message->created_at)) }}
                                    </span>
                                </div>
                                <img class="direct-chat-img" src="https://icons.veryicon.com/256/System/Android%201/Users%20user.png" alt="message user image">
                                <div class="direct-chat-text">{{$message->message}}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
                
                <div class="box-footer">
                    <form action="{{ route('messages.store',$user_id) }}" method="post">
                        @csrf
                        <div class="input-group">
                            <input type="hidden" name="to" value="{{ $user_id }}">
                            <input type="text" name="message" placeholder="Type Message ..." class="form-control" required="" autocomplete="off">
                            <span class="input-group-btn"><button type="submit" class="btn btn-success btn-flat">Send</button></span>
                        </div>
                    </form>
                </div>
            </div>
            <a href="{{ route('messages.index') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Kembali</a>
        </div>
    </div>
@endsection