@extends('adminlte::page')

@section('title', 'Message')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                  	<h3 class="box-title">Semua Members</h3>
                  	<div class="box-tools pull-right">
                    	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    	</button>
                  	</div>
                </div>
                <div class="box-body no-padding">
                  	<ul class="users-list clearfix">
                  		@foreach($data->unique('username') as $item)
                  		<a href="{{ route('messages.show', $item->id) }}">
                    	<li>
                    		@if($item->is_read == 0)
                    			<span class="badge" style="background-color:red; float: left; position: absolute;">1</span>
                    		@endif
                      		<img src="{{ config('app.endpoint_user') . '/storage/'. $item->userdetail->upload_foto }}" style="width: 100px; height: 70px;" alt="User Image">
                      		<a class="users-list-name" href="{{ route('messages.show', $item->id) }}">{{ $item->userdetail->nama_lengkap }}</a>
                      		<span class="users-list-date">
                      			{{ $item->created_at->diffForHumans() }}
                      		</span>
                    	</li>
                    	</a>
                    	@endforeach
                  	</ul>
                </div>
            </div>
	    </div>
	</div>
@endsection