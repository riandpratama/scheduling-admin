@extends('adminlte::page')

@section('title', 'Konfirmasi Penukaran Wbzstockroom')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h4 class="box-title">Konfirmasi Penukaran Wbzstockroom</h4>

		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        <div class="box-body" style="">
		        	@if (Session::has('success'))
			            <div class="alert alert-success">
			                {{ Session::get('success') }}
			            </div>
			        @endif
			        @if (Session::has('warning'))
			            <div class="alert alert-warning">
			                {{ Session::get('warning') }}
			            </div>
			        @endif
	            	<div class="col-md-12 table-responsive">
	            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
		                            	<center>No.</center>
			                        </th>
			                        <th>
			                            <center>Tanggal</center>
			                        </th>
			                        <th>
			                            <center>Email</center>
			                        </th>
			                        <th>
			                            <center>Wbz</center>
			                        </th>
			                        <th>
			                            <center>Rupiah</center>
			                        </th>
			                        <th>
			                            <center>Konfirmasi</center>
			                        </th>
			                        <th>
			                            <center>Pembatalan</center>
			                        </th>
			                    </tr>
			                </thead>
			                <tbody>
			                	@foreach($data as $item)
			                	<tr>
			                		<td><center>{{ $loop->iteration }}</center></td>
			                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
			                		<td><center>{{ $item->user->email }}</center></td>
			                		<td><center>{{ number_format($item->wbz, 2) }}</center></td>
			                		<td><center>Rp.{{ number_format($item->rupiah, 0, ',', '.') }}</center></td>
			                		<td>
			                			<center>
			                				@if ($item->status == 0)
			                				<p data-placement="top" data-toggle="tooltip" title="Edit">
			                					<button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="showEdit(this);" data-item="{{$item}}"><span class="glyphicon glyphicon-ok"></span>
			                					</button>
			                				</p>
			                				@else
			                					<span class="glyphicon glyphicon-ok"></span>
			                				@endif
			                			</center>
			                		</td>
			                		<td>
			                			<form action="{{ route('saldo-wbz.destroy', $item->id) }}" style="display: inline;" method="get">
		                					<button class="btn btn-warning btn-xs" onclick="return confirm('Anda yakin ingin pembatalan?')">
		                						<span class="glyphicon glyphicon-remove"></span>
		                					</button>
		                				</form>
			                		</td>
			                	</tr>
			                	@endforeach
			                </tbody>
			            </table>
	          		</div>
		      	</div>
		    </div>
		</div>
	</div>

	<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
	          <div class="modal-header">
	          	<h4 class="box-title">Konfirmasi Penukaran wbzstockroom</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		      </div>
		          <div class="modal-body">
		          	<form id="formEdit" method="POST" enctype="multipart/form-data">
		          		{{ csrf_field() }}
		                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		                    <label for="email">Email</label>
		                    <input id="email" type="text" readonly="" class="form-control" name="email">
		                    <input id="user_id" type="hidden" name="user_id">
		                </div>

		                <div class="form-group{{ $errors->has('wbz') ? ' has-error' : '' }}">
		                    <label for="wbz">Wbz Stockroom</label>
		                    <input id="wbz" type="text" readonly="" class="form-control" name="wbz">
		                </div>

		                <div class="form-group{{ $errors->has('rupiah') ? ' has-error' : '' }}">
		                    <label for="rupiah">Rupiah</label>
		                    <input id="rupiah" type="text" readonly="" class="form-control" name="rupiah">
		                </div>

		                <div class="modal-footer">

		        			<button type="submit" class="btn btn-success" onclick="return confirm('Anda yakin ingin konfirmasi?')"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Verifikasi</button>

		        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div>  
		      	  	</form>
		      	  </div>
	        </div>
	  	</div>
	</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });

    var format = function(num){
	    var str = num.toString().replace("Rp ", ""), parts = false, output = [], i = 1, formatted = null;
	    if(str.indexOf(".") > 0) {
	        parts = str.split(".");
	        str = parts[0];
	    }
	    str = str.split("").reverse();
	    for(var j = 0, len = str.length; j < len; j++) {
	        if(str[j] != ",") {
	            output.push(str[j]);
	            if(i%3 == 0 && j < (len - 1)) {
	                output.push(".");
	            }
	            i++;
	        }
	    }
	    formatted = output.reverse().join("");
	    return("Rp " + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
	};

    function showEdit(button){
        var item = $(button).data('item');
        $('form#formEdit').attr('action','{{ url("saldo-wbz") }}/'+item.id+'/update');
        $('#formEdit .form-group #user_id').val(item.user.id);
        $('#formEdit .form-group #email').val(item.user.email);
        $('#formEdit .form-group #wbz').val(item.wbz);
        $('#formEdit .form-group #rupiah').val(format(item.rupiah));
    }
</script>
@endsection