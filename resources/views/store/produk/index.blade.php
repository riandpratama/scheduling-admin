@extends('adminlte::page')

@section('title', 'Data Produk')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
            	<div class="box-header with-border">
	              	<h3 class="box-title">Data Produk</h3>
		            <div class="box-tools pull-right">
		                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                </button>
		            </div>
	            </div>
		        <div class="box-body">
	            <a href="{{ route('produk.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Produk</a>
	            <div style="border:1px solid green; margin-bottom: 10px; margin-top: 10px;"></div>
	            	<div class="col-sm-12 table-responsive">
		                <table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
			                            <center>No.</center>
			                        </th>
			                        <th>
			                            <center>Nama Produk</center>
			                        </th>
			                        <th>
			                            <center>WBZStockroom</center>
			                        </th>
			                        <th>
			                            <center>Rupiah</center>
			                        </th>
			                        <th>
			                            <center>Tanggal</center>
			                        </th>
									<th>
			                            <center>Konfirmasi</center>
			                        </th>
			                    </tr>
			                </thead>
			                <tbody>
			                	@foreach($produk as $item)
			                	<tr>
			                		<td><center>{{ $loop->iteration }}.</center></td>
			                		<td><center>{{ $item->nama_produk }}</center></td>
			                		<td><center>{{ $item->harga_coin }}</center></td>
			                		<td><center>{{ number_format($item->harga_rupiah,2) }}</center></td>
			                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
			                		<td>
			                			<center>
			                				<a href="{{ route('produk.edit', $item->id) }}" class="btn btn-warning btn-xs" >
			                					<i class="fa fa-edit"> </i>
			                				</a>
			                			
			                			<form action="{{ route('produk.destroy', $item->id) }}" method="GET" style="display:inline-block;">
                                             <button type="submit" class="btn btn-danger btn-xs" data-toggle="modal" onclick="return confirm('Anda yakin ingin menghapus?')"> <i class="fa fa-trash"> </i> </button>
                                        </form>
                                        </center>
			                		</td>
			                	</tr>
			                	@endforeach
			                </tbody>
	            		</table>
	            	</div>
		        </div>
	    	</div>
        </div>
    </div>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
	<script type="text/javascript">
	    $(function () {
	        $('#datatables').DataTable();
	    });
	</script>

	<script>
		$(document).ready(function() {
		    $('.js-select2').select2();
		});
	</script>
@stop