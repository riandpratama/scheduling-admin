@extends('adminlte::page')

@section('title', 'Produk')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/css/lightbox.css">
@section('content')

	<section class="content">
        <div class="row">   
            <div class="col-lg-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h2 class="box-title"><b> Halaman Produk Foto</b></h2>
                    </div>
                    <div class="box-body">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <small>Semua Foto berada disini.</small>
                                </h2>
                                <br>
                                <a href="{{ route('produk.edit', $produk->id) }}" class="btn btn-success btn-sm"> <i class="fa fa-arrow-left"></i> Kembali</a>
                            </div>
                            <hr>
                            <div class="body">
                                <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                    
                                    <form action="{{ route('produk.storeDetailProduk', $produk->id) }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="col-md-3">
                                            <label for="">Upload Image</label>
                                            <input type="file" name="image_detail[]" multiple="" class="form-control" required="">
                                        </div>
                                        <br>
                                        <div class="col-md-3">
                                            <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                        </div>
                                    </form>
                                     
                                    <hr>
                                    @foreach ($produk->produkImage as $item)
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <form action="{{ route('produk.destroyDetailProduk', $item->id) }}" method="GET" style="display:inline-block;">
                                            <a href="{{ asset('storages/images/detail/500/'.$item->image_path) }}" data-lightbox="mygallery" >
                                                <img src="{{ asset('storages/images/detail/500/'.$item->image_path) }}" class="img-responsive" >
                                            </a>
                                            <button title="Delete" class="btn btn-danger btn-block btn-sm" type="submit">(x) Hapus</button>
                                        </form>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/js/lightbox.min.js"></script>
@stop