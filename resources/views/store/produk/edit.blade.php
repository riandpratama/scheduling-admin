@extends('adminlte::page')

@section('title', 'Edit Data Produk')

@section('content')
	
<div class="box box-success">
	<div class="box-header with-border">
		<h2 class="box-title"><b>Edit Produk/Jasa</b></h2>
	</div>
	<div class="box-body">
		<div class="box-body table-responsive ">
        	<form class="form-horizontal" action="{{ route('produk.update', $produk->id) }}" method="POST" enctype= multipart/form-data>
	            @csrf

	            <div class=" clearfix">
	                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                    <label for="email_address_2">Nama Penjual</label>
	                </div>
	                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                    <div class="form-group">
	                        <div class="form-line">
	                            <input type="text" name="user_id" id="" class="form-control" value="Administrator" readonly="">
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class=" clearfix">
	                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                    <label for="">Kategori</label>
	                </div>
	                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                    <div class="form-group">
	                        <div class="form-line">
	                            <select name="kategori" class="form-control">
	                                <option value="produk" @if($produk->kategori == 'produk') selected="" @endif>Produk</option>
	                                <option value="jasa" @if($produk->kategori == 'jasa') selected="" @endif>Jasa</option>
	                            </select>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class=" clearfix">
	                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                    <label for="">Produk/Jasa</label>
	                </div>
	                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                    <div class="form-group">
	                        <div class="form-line">
	                            <input type="text" name="nama_produk" id="" class="form-control" value="{{ $produk->nama_produk }}" autocomplete="off">
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class=" clearfix">
	                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                    <label for="">Harga Rupiah</label>
	                </div>
	                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                    <div class="form-group">
	                        <div class="form-line">
	                            <input type="number" name="harga_rupiah" id="harga_rupiah" class="form-control idr" value="{{ $produk->harga_rupiah }}" autocomplete="off">
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class=" clearfix">
	                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                    <label for="">Harga WBZ</label>
	                </div>
	                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                    <div class="form-group">
	                        <div class="form-line">
	                            <input type="text" name="harga_coin" id="harga_coin" class="form-control coin" readonly="" value="{{ $produk->harga_coin }}" placeholder="Masukkan Harga WBZ" required="" autocomplete="off">
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class=" clearfix">
	                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                    <label for="stok">Jumlah Stok</label>
	                </div>
	                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                    <div class="form-group">
	                        <div class="form-line">
	                            <input type="number" name="stok" id="stok" class="stok form-control" value="{{ $produk->stok }}" autocomplete="off">
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class=" clearfix">
	                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                    <label for="">Foto Produk Sampul</label>
	                </div>
	                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-7">
	                    <div class="form-group">
	                        <div class="form-line">
	                            <input type="file" name="image" class="form-control">
	                        </div>
	                    </div>
	                    <img src="{{ asset('storages/images/500/'. $produk->image_sampul) }}" style="width: 100%;'">
	                </div>
	            </div>

	            <div class=" clearfix">
	                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
	                    <label for="">Foto Produk Detail</label>
	                </div>
	                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
	                    <div class="form-group">
	                        <div class="form-line">
	                            <a href="{{ route('produk.detailImage', $produk->id) }}" class="btn btn-success">Klik Detail Image Produk</a>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class=" clearfix">
	                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-control-label">
	                    <label for="">Keterangan Produk/Jasa</label>
	                </div>
	            </div>
	            <div class=" clearfix">
	                <div class="col-lg-12 col-md-12">
	                    <div class="form-group">
	                        <div class="form-line">
	                            <textarea name="deskripsi" class="form-control" id="mytextarea" >
	                            	{{ $produk->deskripsi }}
	                            </textarea>
	                        </div>
	                    </div>
	                </div>
	            </div>

	            <div class=" clearfix">
	                <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
	                    <button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Anda yakin ingin mengedit?')">SIMPAN</button>
	                    <a href="{{ route('produk.index') }}" class="btn btn-danger btn-sm">BATAL</a>
	                </div>
	            </div>
	        </form>
        </div>
	</div>
</div>

@endsection

@section('js')
<script src="https://cdn.tiny.cloud/1/w6vzdd6iabbywgbtaez9oijzj2jymhc500w1obh9ftcvxlkd/tinymce/5/tinymce.min.js" referrerpolicy="origin"/></script>
<script>
  	tinymce.init({
    	selector: '#mytextarea'
  	});
</script>
<script type="text/javascript">
    function currency(number) {
        var rupiah = '',
            input = parseInt(number.replace(/[^0-9]/g, ''), 10),
            currency = input.toString().split('').reverse().join('');

        for(var i = 0; i < currency.length; i++) if(i%3 == 0) rupiah += currency.substr(i,3)+'.';

        return rupiah.split('',rupiah.length-1).reverse().join('');
    }

    function implodeCurrency(currency) {
        var curr = '';

        for (var i = 0; i < currency.length; i++) curr += currency[i];

        return parseInt(curr);
    }

    $(".idr").keyup(function(){
        var idr = $(".idr").val();

        var jumlah = (parseInt(idr) / 20000);

        if(isNaN(jumlah)){
            jumlah = 0;
        }

        $(".coin").val(jumlah);

    });

</script>
@endsection