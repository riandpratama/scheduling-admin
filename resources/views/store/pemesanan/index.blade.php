@extends('adminlte::page')

@section('title', 'Konfirmasi Pemesanan')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
            	<div class="box-header with-border">
	              	<h3 class="box-title">Data Pemesanan</h3>
		            <div class="box-tools pull-right">
		                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                </button>
		            </div>
	            </div>
		        <div class="box-body">
		        	@if (Session::has('success'))
			            <div class="alert alert-success">
			                {{ Session::get('success') }}
			            </div>
			        @endif
			        @if (Session::has('warning'))
			            <div class="alert alert-warning">
			                {{ Session::get('warning') }}
			            </div>
			        @endif
	            	<div class="col-sm-12 table-responsive">
		                <table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
			                            <center>No.</center>
			                        </th>
			                        <th>
			                            <center>Username</center>
			                        </th>
			                        <th>
			                            <center>Wbzstockroom</center>
			                        </th>
			                        <th>
			                            <center>Rupiah</center>
			                        </th>
			                        <th>
			                            <center>Tanggal Pemesanan</center>
			                        </th>
			                        <th>
			                            <center>Status</center>
			                        </th>
			                        <th>
			                            <center>Aksi</center>
			                        </th>
			                    </tr>
			                </thead>
			                <tbody>
			                	@foreach($pemesanan as $item)
			                	<tr>
			                		<td><center>{{ $loop->iteration }}.</center></td>
			                		<td><center>{{ $item->pembeli->username }}</center></td>
			                		<td><center>{{ $item->harga_coin }}</center></td>
			                		<td><center>{{ number_format($item->harga_rupiah, 0) }}</center></td>
			                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
			                		<td>
			                			<center>
			                				@if ($item->status == 0)
			                					<span class="badge" style="background-color:blue">Proses</span>
			                				@elseif ($item->status == 1)
			                					<span class="badge" style="background-color:green">Konfirmasi</span>
			                				@elseif ($item->status == 2)
			                					<span class="badge" style="background-color:red">Tolak</span>
			                				@endif
			                			</center>
			                		</td>
			                		<td>
			                			<a href="{{ route('pemesanan.show',$item->id) }}" class='btn btn-xs btn-info'><i class='fa fa-eye'></i> Detail</a>
                                    </td>
			                	</tr>
			                	@endforeach
			                </tbody>
	            		</table>
	            	</div>
		        </div>
	    	</div>
        </div>
    </div>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
	<script type="text/javascript">
	    $(function () {
	        $('#datatables').DataTable();
	    });
	</script>

	<script>
		$(document).ready(function() {
		    $('.js-select2').select2();
		});
	</script>
@stop