@extends('adminlte::page')

@section('title', 'Konfirmasi Pemesanan')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
            	<div class="box-header with-border">
	              	<h3 class="box-title">Detail Pemesanan</h3>
		            <div class="box-tools pull-right">
		                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                </button>
		            </div>
	            </div>
	            
		        <div class="box-body">
	            	<div class="col-sm-12 table-responsive">
	            		
			            <form role="form" method="post" action="{{ route('pemesanan.update', $pemesanan->id) }}">
			            	@csrf
			              	<div class="box-body">
			                	<div class="form-group">
			                 		<label for="faktur">Faktur</label>
			                  		<input type="text" readonly="" class="form-control" id="faktur" value="{{ $pemesanan->kode_pemesanan }}">
			               		</div>
			               		<div class="form-group">
			                  		<label for="username">Tanggal Pemesanan</label>
			                  		<input type="text" readonly="" class="form-control" id="username" value="{{ date('d/m/Y', strtotime($pemesanan->created_at)) }}">
			                	</div>
			                	<div class="form-group">
			                  		<label for="username">Username Pembeli</label>
			                  		<input type="text" readonly="" class="form-control" id="username" value="{{ $pemesanan->pembeli->username }}">
			                	</div>
			                	<div class="form-group">
			                  		<label for="email">Email Pembeli</label>
			                  		<input type="text" readonly="" class="form-control" id="email" value="{{ $pemesanan->pembeli->email }}">
			                	</div>
			                	<div class="form-group">
			                  		<label for="provinsi">Provinsi</label>
			                  		<input type="text" readonly="" class="form-control" id="provinsi" value="{{ $pemesanan->provinsi }}">
			                	</div>
			                	<div class="form-group">
			                  		<label for="kabupaten">Kabupaten</label>
			                  		<input type="text" readonly="" class="form-control" id="kabupaten" value="{{ $pemesanan->kabupaten }}">
			                	</div>
			                	<div class="form-group">
			                  		<label for="alamat">Alamat</label>
			                  		<input type="text" readonly="" class="form-control" id="alamat" value="{{ $pemesanan->alamat_kirim }}">
			                	</div>
			                	<div class="form-group">
			                  		<label for="total_pesan">Total Pemesanan</label>
			                  		<input type="text" readonly="" class="form-control" id="total_pesan" value="{{ $pemesanan->total_pesan }}">
			                	</div>
			                	<div class="form-group">
			                  		<label for="harga_coin">Harga Wbzstockroom</label>
			                  		<input type="text" readonly="" class="form-control" id="harga_coin" value="{{ $pemesanan->harga_coin }}">
			                	</div>
			                	<div class="form-group">
			                  		<label for="harga_rupiah">Harga Rupiah</label>
			                  		<input type="text" readonly="" class="form-control" id="harga_rupiah" value="{{ number_format($pemesanan->harga_rupiah, 0) }}">
			                	</div>

			                	<label for="harga_rupiah">Nama Produk</label>
			                	@foreach($pemesanan->pemesanandetail as $item)
				                	<div class="form-group">
				                  		<input type="text" readonly="" class="form-control" id="harga_rupiah" value="{{ $item->produk->nama_produk }}">
				                	</div>
			                	@endforeach

			                	<label for="harga_rupiah">Total Produk/Jasa</label>
			                	@foreach($pemesanan->pemesanandetail as $item)
				                	<div class="form-group">
				                  		<input type="text" readonly="" class="form-control" id="harga_rupiah" value="Total {{ $item->jumlah_produk }} Produk/Jasa = {{ $item->harga_total_produk }}">
				                	</div>
			                	@endforeach
			                	@if ($pemesanan->status == 0)
			                	<div class="form-group">
			                  		<label for="harga_rupiah">Konfirmasi Pemesanan</label>
			                  		<select class="form-control" name="status">
			                  			<option value="1">Konfirmasi</option>
			                  			<option value="2">Tolak</option>
			                  		</select>
			                	</div>
			                	@endif
			              	</div>
			              	<div class="box-footer">
			              		@if ($pemesanan->status == 0)
			                	<button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin mengkonfirmasi?')"><i class="fa fa-check"></i> Submit</button>
			                	@endif
			                	<a href="{{ route('pemesanan.index') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Kembali</a>
			              	</div>
			            </form>
				        
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection