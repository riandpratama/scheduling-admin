@extends('adminlte::page')

@section('title', 'Konfirmasi Jual Chip')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h3 class="box-title">Konfirmasi Jual Chip</h3>

		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        <div class="box-body" style="">
			        	@if (Session::has('success'))
				            <div class="alert alert-success">
				                {{ Session::get('success') }}
				            </div>
				        @endif
				        @if (Session::has('warning'))
				            <div class="alert alert-warning">
				                {{ Session::get('warning') }}
				            </div>
				        @endif
		            	<div class="col-md-12 table-responsive">
		            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
				                <thead>
				                    <tr>
				                        <th>
			                            	<center>No.</center>
				                        </th>
				                        <th>
				                            <center>Tanggal</center>
				                        </th>
				                        <th>
				                            <center>Email</center>
				                        </th>
				                        <th>
				                            <center>Chip Sebelumnya</center>
				                        </th>
				                        <th>
				                            <center>Chip Diambil</center>
				                        </th>
				                        <th>
				                            <center>Chip Sisa</center>
				                        </th>
				                        <th>
				                            <center>Rupiah</center>
				                        </th>
				                        <th>
				                            <center>Konfirmasi</center>
				                        </th>
				                        <th>
				                            <center>Pembatalann</center>
				                        </th>
				                    </tr>
				                </thead>
				                <tbody>
				                	@foreach($data as $item)
				                	<tr>
				                		<td><center>{{ $loop->iteration }}</center></td>
				                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
				                		<td><center>{{ $item->user->email }}</center></td>
				                		<td><center>{{ $item->coin_saat_ini }}</center></td>
				                		<td><center>{{ $item->coin_diambil }}</center></td>
				                		<td><center>{{ $item->coin_berkurang }}</center></td>
				                		<td><center>Rp.{{ number_format($item->biaya, 0, ',', '.') }}</center></td>
				                		
				                		<td>
				                			<center>
				                				@if ($item->isActive == 1)
				                				<p data-placement="top" data-toggle="tooltip" title="Edit">
				                					<button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="showEdit(this);" data-item="{{$item}}"><span class="glyphicon glyphicon-ok"></span>
				                					</button>
				                				</p>
				                				@else
				                					<span class="glyphicon glyphicon-ok"></span>
				                				@endif
				                			</center>
				                		</td>
				                		<td>
				                			<center>
				                				<form action="{{ route('konfirmasi-ambil-coin.destroy', $item->id) }}" style="display: inline;" method="get">
				                					<button class="btn btn-danger btn-xs" onclick="return confirm('Anda yakin ingin pembatalan?')">
				                						<span class="glyphicon glyphicon-remove"></span>
				                					</button>
				                				</form>
				                			</center>
				                		</td>
				                	</tr>
				                	@endforeach
				                </tbody>
				            </table>
		          		</div>
		        	

		        	<div class="box-footer" style="">
		        	</div>
		      	</div>
		    </div>
		</div>
	</div>

	<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
	          <div class="modal-header">
	          	<h4>Konfirmasi Ambil Coin</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		      </div>
		          <div class="modal-body">
		          	@if (Session::has('success'))
			            <div class="alert alert-success">
			                {{ Session::get('success') }}
			            </div>
			        @endif
			        @if (Session::has('warning'))
			            <div class="alert alert-warning">
			                {{ Session::get('warning') }}
			            </div>
			        @endif

		          	<form id="formEdit" method="POST" enctype="multipart/form-data">
		          		{{ csrf_field() }}
		                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		                    <label for="email">Email</label>
		                    <input id="email" type="text" readonly="" class="form-control" name="email">
		                    <input id="user_id" type="hidden" name="user_id">
		                </div>

		                <div class="form-group{{ $errors->has('coin_saat_ini') ? ' has-error' : '' }}">
		                    <label for="coin_saat_ini">Coin Sebelumnya</label>
		                    <input id="coin_saat_ini" type="text" readonly="" class="form-control" name="coin_saat_ini">
		                </div>

		                <div class="form-group{{ $errors->has('coin_diambil') ? ' has-error' : '' }}">
		                    <label for="coin_diambil">Coin Diambil</label>
		                    <input id="coin_diambil" type="text" readonly="" class="form-control" name="coin_diambil">
		                </div>

		                <div class="form-group{{ $errors->has('coin_berkurang') ? ' has-error' : '' }}">
		                    <label for="coin_berkurang">Coin Sisa</label>
		                    <input id="coin_berkurang" type="text" readonly="" class="form-control" name="coin_berkurang">
		                </div>

		                <div class="form-group{{ $errors->has('biaya') ? ' has-error' : '' }}">
		                    <label for="biaya">Biaya</label>
		                    <input id="biaya" type="text" readonly="" class="form-control" name="biaya">
		                </div>

		                <div class="form-group{{ $errors->has('nama_bank') ? ' has-error' : '' }}">
		                    <label for="nama_bank">Bank</label>
		                    <input id="nama_bank" type="text" readonly="" class="form-control" name="nama_bank">
		                </div>

		                <div class="form-group{{ $errors->has('no_rekening') ? ' has-error' : '' }}">
		                    <label for="no_rekening">Nomor Rekening</label>
		                    <input id="no_rekening" type="text" readonly="" class="form-control" name="no_rekening">
		                </div>
		                
		                <div class="modal-footer">

		        			<button type="submit" class="btn btn-success" onclick="return confirm('Anda yakin ingin konfirmasi?')"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Verifikasi</button>

		        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div>  
		      	  	</form>
		      	  </div>
	        </div>
	  	</div>
	</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });

    var format = function(num){
	    var str = num.toString().replace("Rp ", ""), parts = false, output = [], i = 1, formatted = null;
	    if(str.indexOf(".") > 0) {
	        parts = str.split(".");
	        str = parts[0];
	    }
	    str = str.split("").reverse();
	    for(var j = 0, len = str.length; j < len; j++) {
	        if(str[j] != ",") {
	            output.push(str[j]);
	            if(i%3 == 0 && j < (len - 1)) {
	                output.push(".");
	            }
	            i++;
	        }
	    }
	    formatted = output.reverse().join("");
	    return("Rp " + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
	};

    function showEdit(button){
        var item = $(button).data('item');
        $('form#formEdit').attr('action','{{ url("konfirmasi-ambil-coin/store") }}/'+item.id+'/update');
        $('#formEdit .form-group #user_id').val(item.user.id);
        $('#formEdit .form-group #email').val(item.user.email);
        $('#formEdit .form-group #coin_saat_ini').val(item.coin_saat_ini);
        $('#formEdit .form-group #coin_diambil').val(item.coin_diambil);
        $('#formEdit .form-group #coin_berkurang').val(item.coin_berkurang);
        $('#formEdit .form-group #biaya').val(format(item.biaya));
        $('#formEdit .form-group #nama_bank').val(item.userdetail.nama_bank);
        $('#formEdit .form-group #no_rekening').val(item.userdetail.no_rekening);
    }
</script>
@endsection