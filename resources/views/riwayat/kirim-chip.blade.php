@extends('adminlte::page')

@section('title', 'Riwayat Kirim Chip')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h3 class="box-title">Riwayat Kirim Chip</h3>

		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        <div class="box-body">
	            	<div class="col-md-12 table-responsive">
	            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
			                            <center>No.</center>
			                        </th>
			                        <th>
			                            <center>Tanggal</center>
			                        </th>
			                        <th>
			                            <center>Faktur</center>
			                        </th>
			                        <th>
			                            <center>Pengirim</center>
			                        </th>
			                        <th>
			                            <center>Penerima</center>
			                        </th>
			                        <th>
			                            <center>Chip</center>
			                        </th>
			                        <th>
			                            <center>Rupiah</center>
			                        </th>
			                        <th>
			                            <center>Status</center>
			                        </th>
			                    </tr>
			                </thead>
			                <tbody>
			                	@foreach($data as $item)
			                	<tr>
			                		<td><center>{{ $loop->iteration }}</center></td>
			                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
			                		<td><center>{{ $item->faktur }}</center></td>
			                		<td><center>{{ $item->kirimcoinpenjual->username }}</center></td>
			                		<td><center>{{ $item->kirimcoin->username }}</center></td>
			                		<td><center>{{ $item->coin }}</center></td>
			                		<td><center>Rp.{{ number_format($item->biaya, 0, ',', '.') }}</center></td>
			                		<td>
			                			<center>
			                			@if ($item->isActive == 1)
			                				<p style="color:green;">Terselesaikan</p>
			                			@elseif ($item->isActive == 2)
			                				<p style="color:red;">Dibatalkan</p>
			                			@else
											<p>-</p>
			                			@endif
			                			</center>
			                		</td>
			                	</tr>
			                	@endforeach
			                </tbody>
			            </table>
	          		</div>
		      	</div>
		    </div>
		</div>
	</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });

    function showEdit(button){
        var item = $(button).data('item');
        var uuid = "{{ Auth::user()->id }}";
        $('form#formEdit').attr('action','{{ url("konfirmasi-topup/store") }}/'+uuid+'/'+item.id+'/update');
        $('#formEdit .form-group #faktur').val(item.faktur);
    }
</script>
@endsection