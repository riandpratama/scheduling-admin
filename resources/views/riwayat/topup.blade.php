@extends('adminlte::page')

@section('title', 'Riwayat Top up (Exchanger)')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h3 class="box-title">Riwayat Top up (Exchanger)</h3>

		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        <div class="box-body" style="">
		          	
		            	<div class="col-md-12 table-responsive">
		            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
				                <thead>
				                    <tr>
				                        <th>
				                            <center>No.</center>
				                        </th>
				                        <th>
				                            <center>Faktur</center>
				                        </th>
				                        <th>
				                            <center>Penerima</center>
				                        </th>
				                        <th>
				                            <center>Tanggal</center>
				                        </th>
				                        <th>
				                            <center>Chip</center>
				                        </th>
				                        <th>
				                            <center>Biaya</center>
				                        </th>
				                        <th>
				                            <center>Status</center>
				                        </th>
				                    </tr>
				                </thead>
				                <tbody>
				                	@foreach($data as $item)
				                	<tr>
				                		<td><center>{{ $loop->iteration }}</center></td>
				                		<td><center>{{ $item->faktur }}</center></td>
				                		<td><center>{{ $item->user->username }}</center></td>
				                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
				                		<td><center>{{ $item->coin }}</center></td>
				                		<td><center>Rp.{{ number_format($item->biaya, 0, ',', '.') }}</center></td>
				                		<td>
				                			<center>
				                			@if ($item->isActive == 1)
				                				<p style="color:red;">Belum dibayarkan</p>
				                			@elseif ($item->isActive == 2)
				                				<p style="color:blue;">Proses</p>
				                			@elseif ($item->isActive == 3)
				                				<p style="color:green;">Terkonfirmasi</p>
				                			@else
												<p>-</p>
				                			@endif
				                			</center>
				                		</td>
				                	</tr>
				                	@endforeach
				                </tbody>
				            </table>
		          		</div>
		        	

		        	<div class="box-footer" style="">
		        	</div>
		      	</div>
		    </div>
		</div>
	</div>
@endsection

@section('js')

<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

@endsection