@extends('adminlte::page')

@section('title', 'Konfirmasi Top Up')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h3 class="box-title">Konfirmasi Topup</h3>

		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        @if (Session::has('success'))
		            <div class="alert alert-success">
		                {{ Session::get('success') }}
		            </div>
		        @endif
		        @if (Session::has('warning'))
		            <div class="alert alert-warning">
		                {{ Session::get('warning') }}
		            </div>
		        @endif
			        
		        <div class="box-body" style="">
		          	
		            	<div class="col-md-12 table-responsive">
		            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
				                <thead>
				                    <tr>
				                        <th>
				                            <center>No.</center>
				                        </th>
				                        <th>
				                            <center>Faktur</center>
				                        </th>
				                        <th>
				                            <center>Username</center>
				                        </th>
				                        <th>
				                            <center>Tanggal</center>
				                        </th>
				                        <th>
				                            <center>Status</center>
				                        </th>
				                        <th>
				                            <center>Konfirmasi</center>
				                        </th>
				                        <th>
				                            <center>Pembatalan</center>
				                        </th>
				                    </tr>
				                </thead>
				                <tbody>
				                	@foreach($data as $item)
				                	<tr>
				                		<td><center>{{ $loop->iteration }}</center></td>
				                		<td><center>{{ $item->faktur }}</center></td>
				                		<td><center>{{ $item->user->username }}</center></td>
				                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
				                		<td>
				                			<center>
				                			@if ($item->isActive == 1)
				                				<p style="color:red;">Belum dibayarkan</p>
				                			@elseif ($item->isActive == 2)
				                				<p style="color:blue;">Pending</p>
				                			@elseif ($item->isActive == 3)
				                				<p style="color:green;">Terkonfirmasi</p>
				                			@else
												<p>-</p>
				                			@endif
				                			</center>
				                		</td>
				                		<td>
				                			<center>
				                				@if ($item->isActive == 2)
				                				<p data-placement="top" data-toggle="tooltip" title="Edit">
				                					<button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="showEdit(this);" data-item="{{$item}}"><span class="glyphicon glyphicon-ok"></span>
				                					</button>
				                				</p>
				                				@else
				                					<span class="glyphicon glyphicon-ok"></span>
				                				@endif
				                			</center>
				                		</td>
				                		<td>
				                			<center>
				                				<form action="{{ route('konfirmasi-topup.destroy', $item->id) }}" style="display: inline;" method="get">
				                					<button class="btn btn-danger btn-xs" onclick="return confirm('Anda yakin ingin pembatalan?')">
				                						<span class="glyphicon glyphicon-remove"></span>
				                					</button>
				                				</form>
				                			</center>
				                		</td>
				                	</tr>
				                	@endforeach
				                </tbody>
				            </table>
		          		</div>
		        	

		        	<div class="box-footer" style="">
		        	</div>
		      	</div>
		    </div>
		</div>
	</div>

	<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
	          <div class="modal-header">
	          	<h4>Konfirmasi Topup</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		      </div>
		          <div class="modal-body">

		          	<form id="formEdit" method="POST" enctype="multipart/form-data">
		          		{{ csrf_field() }}
		                <div class="form-group{{ $errors->has('faktur') ? ' has-error' : '' }}">
		                    <label for="faktur">Faktur</label>
		                    <input id="faktur" type="text" readonly="" class="form-control" name="faktur">
		                </div>

		                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		                    <label for="email">Email</label>
		                    <input id="email" type="text" readonly="" class="form-control" name="email">
		                </div>

		                <div class="form-group{{ $errors->has('coin') ? ' has-error' : '' }}">
		                    <label for="coin">Coin</label>
		                    <input id="coin" type="text" readonly="" class="form-control" name="coin">
		                </div>

		                <div class="form-group{{ $errors->has('biaya') ? ' has-error' : '' }}">
		                    <label for="biaya">Biaya</label>
		                    <input id="biaya" type="text" readonly="" class="form-control" name="biaya">
		                </div>

		                <div class="form-group{{ $errors->has('upload_bukti') ? ' has-error' : '' }}">
		                    <label for="upload_bukti">Bukti Transfer/Scan Bukti</label>
		                    <br>
	        				<p><image src="" id="upload_bukti" style="width: 50%;"> </p>
		                </div>
		                
		                <div class="modal-footer">

		        			<button type="submit" class="btn btn-success" onclick="return confirm('Anda yakin ingin konfirmasi pelanggan?')">
		        				<span class="glyphicon glyphicon-check" aria-hidden="true"></span> Verifikasi
		        			</button>

		        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div>  
		      	  	</form>
		      	  </div>
	        </div>
	  	</div>
	</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });

    var format = function(num){
	    var str = num.toString().replace("Rp ", ""), parts = false, output = [], i = 1, formatted = null;
	    if(str.indexOf(".") > 0) {
	        parts = str.split(".");
	        str = parts[0];
	    }
	    str = str.split("").reverse();
	    for(var j = 0, len = str.length; j < len; j++) {
	        if(str[j] != ",") {
	            output.push(str[j]);
	            if(i%3 == 0 && j < (len - 1)) {
	                output.push(".");
	            }
	            i++;
	        }
	    }
	    formatted = output.reverse().join("");
	    return("Rp " + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
	};

    function showEdit(button){
        var item = $(button).data('item');
        var uuid = "{{ Auth::user()->id }}";
        $('form#formEdit').attr('action','{{ url("konfirmasi-topup/store") }}/'+item.id+'/update');
        $('#formEdit .form-group #faktur').val(item.faktur);
        $('#formEdit .form-group #email').val(item.user.email);
        $('#formEdit .form-group #coin').val(item.coin);
        $('#formEdit .form-group #biaya').val(format(item.biaya));
        document.getElementById("upload_bukti").innerHTML = item.upload_bukti;
        $('#upload_bukti').attr('src','http://scheduling.localhost/storage/'+item.upload_bukti);
    }
</script>
@endsection