@extends('adminlte::page')

@section('title', 'Ubah Password Member')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
		        <div class="box-header with-border">
		          	<h3 class="box-title">Ubah Password Member</h3>

		          	<div class="box-tools pull-right">
		            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		          	</div>
		        </div>

		        @if (Session::has('success'))
		            <div class="alert alert-success">
		                {{ Session::get('success') }}
		            </div>
		        @endif
		        @if (Session::has('warning'))
		            <div class="alert alert-warning">
		                {{ Session::get('warning') }}
		            </div>
		        @endif
			        
		        <div class="box-body" style="">
		          	
		            	<div class="col-md-12 table-responsive">
		            		<table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
				                <thead>
				                    <tr>
				                        <th>
				                            <center>No.</center>
				                        </th>
				                        <th>
				                            <center>Email</center>
				                        </th>
				                        <th>
				                            <center>Tanggal</center>
				                        </th>
				                        <th>
				                            <center>Konfirmasi </center>
				                        </th>
				                    </tr>
				                </thead>
				                <tbody>
				                	@foreach($data as $item)
				                	<tr>
				                		<td><center>{{ $loop->iteration }}</center></td>
				                		<td><center>{{ $item->email }}</center></td>
				                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
				                		<td>
				                			<center>
				                				<p data-placement="top" data-toggle="tooltip" title="Edit">
				                					<button class="btn btn-success btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="showEdit(this);" data-item="{{$item->token}}"><span class="glyphicon glyphicon-ok"></span>
				                					</button>
				                				</p>
				                			</center>
				                		</td>
				                	</tr>
				                	@endforeach
				                </tbody>
				            </table>
		          		</div>
		        	

		        	<div class="box-footer" style="">
		        	</div>
		      	</div>
		    </div>
		</div>
	</div>

	<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered modal-lg">
	    	<div class="modal-content">
	          <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		      </div>
		          <div class="modal-body">

		          	<form id="formEdit" method="POST" enctype="multipart/form-data">
		          		{{ csrf_field() }}
		                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		                    <p><b>Anda yakin ingin merubah password member?</b></p>
		                </div>
		                
		                <div class="modal-footer">

		        			<button type="submit" class="btn btn-success">
		        				<span class="glyphicon glyphicon-check" aria-hidden="true"></span> Ubah Password
		        			</button>

		        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div>  
		      	  	</form>
		      	  </div>
	        </div>
	  	</div>
	</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });

    function showEdit(button){
        var item = $(button).data('item');
        
        var uuid = "{{ Auth::user()->id }}";
        $('form#formEdit').attr('action','{{ url("reset-password-member") }}/'+item+'/update');
        $('#formEdit .form-group #email').val(item);
    }
</script>
@endsection