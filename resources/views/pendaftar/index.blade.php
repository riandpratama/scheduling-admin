@extends('adminlte::page')

@section('title', 'Pendaftar')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
            	<div class="box-header with-border">
	              	<h3 class="box-title">Pendaftaran User Baru</h3>
		            <div class="box-tools pull-right">
		                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                </button>
		            </div>
	            </div>
		        <div class="box-body">
	            	<div class="col-sm-12 table-responsive">
		                <table id="customers-datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
			                            <center>No.</center>
			                        </th>
			                        <th>
			                            <center>Email</center>
			                        </th>
			                        <th>
			                            <center>Nama</center>
			                        </th>
			                        <th>
			                            <center>Tanggal Daftar</center>
			                        </th>
			                        <th>
			                            <center>Verifikasi</center>
			                        </th>
			                    </tr>
			                </thead>
	            		</table>
	            	</div>
		        </div>
	    	</div>
        </div>
    </div>

    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	          	<div class="modal-header">
	          		<h4><i class="fa fa-check"></i> Detail Data</h4>
		        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		      	</div>
	          	<div class="modal-body">
	          		<form id="formEdit" method="POST" enctype="multipart/form-data">
	          			{{ csrf_field() }}
	                	<div class="form-group{{ $errors->has('nama_lengkap') ? ' has-error' : '' }}">
		                    <label for="nama_lengkap">Nama Lengkap</label>
		                    <input id="nama_lengkap" type="text" class="form-control" name="nama_lengkap" value="{{ old('nama_lengkap') }}" disabled="">
		                    @if ($errors->has('nama_lengkap'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('nama_lengkap') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('no_ktp') ? ' has-error' : '' }}">
		                    <label for="no_ktp">No KTP</label>
		                    <input id="no_ktp" type="text" class="form-control" name="no_ktp" value="{{ old('no_ktp') }}" disabled="">
		                    @if ($errors->has('no_ktp'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('no_ktp') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
		                    <label for="username">Username</label>
		                    <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" disabled="">
		                    @if ($errors->has('username'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('username') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('sponsor') ? ' has-error' : '' }}">
		                    <label for="sponsor">Sponsor</label>
		                    <input id="sponsor" type="text" class="form-control" name="sponsor" value="{{ old('sponsor') }}" disabled="">
		                    @if ($errors->has('sponsor'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('sponsor') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('no_hp') ? ' has-error' : '' }}">
		                    <label for="no_hp">No. Telpon/No. Handphone</label>
		                    <input id="no_hp" type="text" class="form-control" name="no_hp" value="{{ old('no_hp') }}" disabled="">
		                    @if ($errors->has('no_hp'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('no_hp') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('tgl_lahir') ? ' has-error' : '' }}">
		                    <label for="tgl_lahir">Tanggal Lahir</label>
		                    <input id="tgl_lahir" type="date" class="form-control" name="tgl_lahir" value="{{ old('tgl_lahir') }}" disabled="">
		                    @if ($errors->has('tgl_lahir'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('tgl_lahir') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
		                    <label for="alamat">Alamat</label>
		                    <input id="alamat" type="text" class="form-control" name="alamat" value="{{ old('alamat') }}" disabled="">
		                    @if ($errors->has('alamat'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('alamat') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('nama_bank') ? ' has-error' : '' }}">
		                    <label for="nama_bank">Nama Bank</label>
		                    <input id="nama_bank" type="text" class="form-control" name="nama_bank" value="{{ old('nama_bank') }}" disabled="">
		                    @if ($errors->has('nama_bank'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('nama_bank') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('nama_cabang') ? ' has-error' : '' }}">
		                    <label for="nama_cabang">Nama Cabang</label>
		                    <input id="nama_cabang" type="text" class="form-control" name="nama_cabang" value="{{ old('nama_cabang') }}" disabled="">
		                    @if ($errors->has('nama_cabang'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('nama_cabang') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('no_rekening') ? ' has-error' : '' }}">
		                    <label for="no_rekening">No. Rekening</label>
		                    <input id="no_rekening" type="text" class="form-control" name="no_rekening" value="{{ old('no_rekening') }}" disabled="">
		                    @if ($errors->has('no_rekening'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('no_rekening') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('swift_code') ? ' has-error' : '' }}">
		                    <label for="swift_code">Swift Code</label>
		                    <input id="swift_code" type="text" class="form-control" name="swift_code" value="{{ old('swift_code') }}" disabled="">
		                    @if ($errors->has('swift_code'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('swift_code') }}</strong>
		                        </span>
		                    @endif
		                </div>

		                <div class="form-group">
		                    <label for="image_foto">Foto Identitas</label>
		                    <p><image src="" id="image_foto" style="width: 50%;"> </p>
		                </div>

		                <div class="form-group">
		                    <label for="image_ktp">Foto KTP</label>
		                    <p><image src="" id="image_ktp" style="width: 50%;"> </p>
		                </div>

		                <div class="form-group">
		                    <label for="image_pembayaran">Foto Bukti Pembayaran</label>
		                    <p><image src="" id="image_pembayaran" style="width: 50%;"> </p>
		                </div>

		                <div class="modal-footer">
		        			<button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin memverifikasi?')"><span class="glyphicon glyphicon-check"></span> Verifikasi</button>
		        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div>  
	            	</form>
	            	<div>
	            		<form id="formDelete" action="" method="get">
	            			<button type="submit" class="btn btn-block btn-warning" onclick="return confirm('Anda yakin ingin menghapus akun?')"><i class="fa fa-trash"></i> Hapus Akun</button>
	            		</form>
	            	</div>
	        	</div>
		    </div>
		</div>
	</div>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
	<script type="text/javascript">
	    $(function () {
	        $('#customers-datatables').DataTable({
	            paging: true,
	            searching: { "regex": true },
	            lengthMenu: [ [10, 20, 25, 50, 100 -1], [10, 20, 25, 50, 100, "All"] ],
	            pageLength: 10,
	            serverSide: true,
	            processing: true,
	            ajax: '{{ route('pendaftar.getdata') }}',
	            columns: [
	                {data: 'DT_RowIndex'},
	                {data: 'email'},
	                {data: 'nama_lengkap'},
	                {data: 'created_at'},
	                {data: 'edit'},
	            ],
	        });
	    });
	</script>
	<script type="text/javascript">
		edit = (button) => {
	        var item = $(button).data('item');
	        // console.log(item);
	        $('form#formEdit').attr('action','{{ url("pendaftar") }}/'+item.id+'/update');
	        $('#formEdit .form-group #nama_lengkap').val(item.nama_lengkap);
	        $('#formEdit .form-group #username').val(item.username);
	        $('#formEdit .form-group #no_ktp').val(item.no_ktp);
	        $('#formEdit .form-group #sponsor').val(item.nama_sponsor);
	        $('#formEdit .form-group #no_hp').val(item.no_hp);
	        $('#formEdit .form-group #birth').val(item.birth);
	        $('#formEdit .form-group #tgl_lahir').val(item.tgl_lahir);
	        $('#formEdit .form-group #alamat').val(item.alamat);
	        $('#formEdit .form-group #nama_bank').val(item.nama_bank);
	        $('#formEdit .form-group #nama_cabang').val(item.nama_cabang);
	        $('#formEdit .form-group #no_rekening').val(item.no_rekening);
	        $('#formEdit .form-group #swift_code').val(item.swift_code);
	        document.getElementById("image_foto").innerHTML = item.upload_foto;
        	$('#image_foto').attr('src','http://scheduling.localhost/storage/'+item.upload_foto);
        	document.getElementById("image_ktp").innerHTML = item.upload_ktp;
        	$('#image_ktp').attr('src','http://scheduling.localhost/storage/'+item.upload_ktp);
        	document.getElementById("image_pembayaran").innerHTML = item.upload_pembayaran;
        	$('#image_pembayaran').attr('src','http://scheduling.localhost/storage/'+item.upload_pembayaran);

        	$('form#formDelete').attr('action','{{ url("pendaftar") }}/'+item.id+'/delete');
	    }
	</script>

	<script>
		$(document).ready(function() {
		    $('.js-select2').select2();
		});
	</script>
@stop