Hi, {{ $data->user->userdetail->nama_lengkap }} (#{{ $data->user->username }}) <br>
<br> 
Kirim Saldo WbzStockroom Anda telah di konfirmasi oleh pihak administrator.
<br>

Wbzstockroom Informasi: <br>
<br>
Jumlah WbzStockroom: {{ $data->wbz }} <br>
Jumlah Rupiah: Rp {{ number_format($data->rupiah, 2, ',', '.') }} <br>
Pada Tanggal: {{ date('d/m/Y', strtotime($data->updated_at)) }}<br>
<br>
Transaksi Berhasil <br>
<br>
<hr>
<p style="color:gray; font-size: 12px;">
<i>
Peringatan:<br>
Email ini dikirim secara otomatis oleh sistem – mohon untuk tidak dibalas. Jika Anda memiliki pertanyaan mengenai email ini, silakan hubungi kami. <br>
Informasi yang terkandung di dalam email ini bersifat rahasia dan hanya ditujukan kepada penerima yang namanya tersebut di atas. <br>
Dilarang keras untuk membagikan bagian mana pun dari pesan ini kepada pihak ketiga mana pun, tanpa persetujuan tertulis dari pengirim.
</i>
</p>
