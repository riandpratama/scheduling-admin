<p style="text-align: center;">Akun Anda Telah diverifikasi oleh Administrator</p>

<p> Hi {{ $user->email }}, Selamat bergabung di Wallbroz District. </p>

<p style="color:gray;"> Mulai saat ini chip anda sudah bisa bertambah perhari nya. </p>

<br>
<br>
<hr>
<p style="color:gray; font-size: 12px;">
<i>
Peringatan:<br>
Email ini dikirim secara otomatis oleh sistem – mohon untuk tidak dibalas. Jika Anda memiliki pertanyaan mengenai email ini, silakan hubungi kami. <br>
Informasi yang terkandung di dalam email ini bersifat rahasia dan hanya ditujukan kepada penerima yang namanya tersebut di atas. <br>
Dilarang keras untuk membagikan bagian mana pun dari pesan ini kepada pihak ketiga mana pun, tanpa persetujuan tertulis dari pengirim.
</i>
</p>