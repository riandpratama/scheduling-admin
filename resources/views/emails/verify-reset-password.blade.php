Hi, {{ $email }}<br>
<br> 
Permintaan anda <b>Lupa Password telah di setujui Administrator</b>
<br>
<br>
Masukkan password sama dengan email anda, dengan email {{ $email }}.
<br>
Harap setelah melakukan <b>Lupa Password</b>, anda segera merubah password anda di menu Ubah Password.
<br>
<br>
<hr>
<p style="color:gray; font-size: 12px;">
<i>
Peringatan:<br>
Email ini dikirim secara otomatis oleh sistem – mohon untuk tidak dibalas. Jika Anda memiliki pertanyaan mengenai email ini, silakan hubungi kami. <br>
Informasi yang terkandung di dalam email ini bersifat rahasia dan hanya ditujukan kepada penerima yang namanya tersebut di atas. <br>
Dilarang keras untuk membagikan bagian mana pun dari pesan ini kepada pihak ketiga mana pun, tanpa persetujuan tertulis dari pengirim.
</i>
</p>
