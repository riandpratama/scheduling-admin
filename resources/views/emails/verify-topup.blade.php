{{-- Topup Berhasil dan telah di verifikasi oleh administrator.
Coin Anda Sebesar {{ $user->coin }} dengan biaya Rp {{ number_format($user->biaya, 0, ',', '.') }}, telah masuk. --}}

Hi, {{ $user->user->userdetail->nama_lengkap }} (#{{ $user->user->username }}) <br>
<br> 
Chip Anda telah di verifikasi oleh pihak administrator.
<br>

Top up Informasi: <br>
<br>
Faktur: {{ $user->faktur }} <br>
Nilai Chip: {{ $user->coin }} <br>
Nilai Rupiah: Rp {{ number_format($user->biaya, 2, ',', '.') }} <br>
Pada Tanggal: {{ date('d/m/Y', strtotime($user->created_at)) }}<br>
<br>
Transaksi Berhasil
<br>
<br>
<hr>
<p style="color:gray; font-size: 12px;">
<i>
Peringatan:<br>
Email ini dikirim secara otomatis oleh sistem – mohon untuk tidak dibalas. Jika Anda memiliki pertanyaan mengenai email ini, silakan hubungi kami. <br>
Informasi yang terkandung di dalam email ini bersifat rahasia dan hanya ditujukan kepada penerima yang namanya tersebut di atas. <br>
Dilarang keras untuk membagikan bagian mana pun dari pesan ini kepada pihak ketiga mana pun, tanpa persetujuan tertulis dari pengirim.
</i>
</p>
