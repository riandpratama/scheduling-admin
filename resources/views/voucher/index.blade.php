@extends('adminlte::page')

@section('title', 'Pendaftar')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
            	<div class="box-header with-border">
	              	<h3 class="box-title">List Voucher</h3>
		            <div class="box-tools pull-right">
		                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                </button>
		            </div>
	            </div>
		        <div class="box-body">
		        	<a href="" class="btn btn-primary" style="margin-bottom: 15px;" data-title="Tambah" data-toggle="modal" data-target="#tambah">
		        		<i class="fa fa-plus"></i>&nbsp;Buat Voucher 
		        	</a>
	            	<div class="col-sm-12 table-responsive">
		                <table id="datatables" class="table table-responsive table-striped table-bordered table-hover" width="100%" style="text-align: center;">
			                <thead>
			                    <tr>
			                        <th>
			                            <center>No.</center>
			                        </th>
			                        <th>
			                            <center>Email Pemberi Voucher</center>
			                        </th>
			                        <th>
			                            <center>Email Penerima Voucher</center>
			                        </th>
			                        <th>
			                            <center>Kode Sponsor</center>
			                        </th>
			                        <th>
			                            <center>Kode Voucher</center>
			                        </th>
			                        <th>
			                            <center>Status</center>
			                        </th>
			                        <th>
			                            <center>Tanggal Dibuat</center>
			                        </th>
			                        <th>
			                            <center>Delete</center>
			                        </th>
			                    </tr>
			                </thead>
			                <tbody>
			                	@foreach($voucher as $item)
			                	<tr>
			                		<td><center>{{ $loop->iteration }}</center></td>
			                		<td><center>{{ $item->userPemberi->email }}</center></td>
			                		<td><center>{{ $item->userPenerima->email }}</center></td>
			                		<td><center>{{ $item->sponsor['nama_sponsor'] }}</center></td>
			                		<td><center>{{ $item->kode_vouchers }}</center></td>
			                		<td>
			                			<center>{{ ($item->isActive == 1) ? 'Belum digunakan' : 'Sudah Digunakan' }}</center>
			                		</td>
			                		<td><center>{{ date('d/m/Y', strtotime($item->created_at)) }}</center></td>
			                		<td>
			                			@if ($item->isActive == 0)
			                				<i class="fa fa-check"></i>
			                			@else
			                			<form action="{{ route('voucher.destroy', $item->id) }}" method="get">
				                			<button type="submit" onclick="return confirm('Anda yakin ingin menghapus?')"
	                                            class='btn btn-xs btn-danger'><i class='fa fa-trash'></i>
	                                        </button>
                                        </form>
                                        @endif
                                    </td>
			                	</tr>
			                	@endforeach
			                </tbody>
	            		</table>
	            	</div>
		        </div>
	    	</div>
        </div>
    </div>

    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	          <div class="modal-header">
	          	<h4><i class="fa fa-plus"></i> Tambah Voucher</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		      </div>
		          <div class="modal-body">
		          	<form action="{{ route('voucher.store') }}" method="POST" enctype="multipart/form-data">
	              		{{ csrf_field() }}
		                <div class="form-group{{ $errors->has('user_id_pemberi') ? ' has-error' : '' }}">
		                    <label for="user_id_pemberi">Pilih User pemeberi voucher (Email) <i style="color: red;">*</i></label><br>
		                    <select class="form-control js-select2" name="user_id_pemberi" style="width: 100%" autofocus required>
	                        	@foreach($user as $item)
	                        		<option value="{{ $item->id }}">{{ $item->email }}</option>
	                        	@endforeach
	                        </select>
		                    @if ($errors->has('user_id_pemberi'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('user_id_pemberi') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="form-group{{ $errors->has('user_id_pemberi') ? ' has-error' : '' }}">
		                    <label for="user_id_pemberi">Tanggal Pembuatan Voucher </label><br>
		                    <input type="text" class="form-control" readonly="" value="{{ date('d/m/Y') }}">
		                    @if ($errors->has('user_id_pemberi'))
		                        <span class="help-block">
		                            <strong>{{ $errors->first('user_id_pemberi') }}</strong>
		                        </span>
		                    @endif
		                </div>
		                <div class="modal-footer">
		        			<button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin menambahkan voucher?')"><span class="glyphicon glyphicon-save"></span> Submit</button>
		        			<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
		      	  		</div>
		            </form>
		        </div>
		    </div>
		</div>
	</div>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
	<script type="text/javascript">
	    $(function () {
	        $('#datatables').DataTable();
	    });
	</script>

	<script>
		$(document).ready(function() {
		    $('.js-select2').select2();
		});
	</script>
@stop