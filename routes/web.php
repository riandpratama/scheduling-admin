<?php

Route::get('/migrate', function() {
	Artisan::call('migrate');
});

Route::get('/cache', function(){
	Artisan::call('config:cache');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@postLogin')->name('login.post');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
	Route::get('pendaftar', 'PendaftarController@index')->name('pendaftar');
	Route::get('pendaftar/data', 'PendaftarController@getData')->name('pendaftar.getdata');
	Route::post('pendaftar/{id}/update', 'PendaftarController@update')->name('pendaftar.update');
	Route::get('pendaftar/{id}/delete', 'PendaftarController@delete')->name('pendaftar.delete');

	Route::get('voucher', 'VoucherController@index')->name('voucher');
	Route::get('voucher/data', 'VoucherController@getData')->name('voucher.getdata');
	Route::post('voucher/store', 'VoucherController@store')->name('voucher.store');
	Route::get('voucher/destroy/{id}', 'VoucherController@destroy')->name('voucher.destroy');

	Route::get('konfirmasi-topup', 'KonfirmasiTopupController@index')->name('konfirmasi-topup');
	Route::post('konfirmasi-topup/store/{id}/update', 'KonfirmasiTopupController@store')->name('konfirmasi-topup.store');
    Route::get('konfirmasi-topup/{id}/destroy', 'KonfirmasiTopupController@destroy')->name('konfirmasi-topup.destroy');

	Route::get('konfirmasi-ambil-coin', 'KonfirmasiAmbilCoinController@index')->name('konfirmasi-ambil-coin');
	Route::post('konfirmasi-ambil-coin/store/{id}/update', 'KonfirmasiAmbilCoinController@store')->name('konfirmasi-ambil-coin.store');
    Route::get('konfirmasi-ambil-coin/{id}/destroy', 'KonfirmasiAmbilCoinController@destroy')->name('konfirmasi-ambil-coin.destroy');

	Route::get('konfirmasi-jual-beli-coin', 'KonfirmasiJualBeliController@index')->name('konfirmasi-jual-beli-coin');
	Route::post('konfirmasi-jual-chip/store/{uuid}/{id}', 'KonfirmasiJualBeliController@store')->name('konfirmasi-jual-beli.store');
	Route::post('konfirmasi-jual-chip/batal/{uuid}/{id}', 'KonfirmasiJualBeliController@batal')->name('konfirmasi-jual-beli.batal');

	Route::get('deskripsi', 'DeskripsiController@index')->name('deskripsi.index');
	Route::post('deskripsi/update-informasi/{id}', 'DeskripsiController@updateInformasi')->name('deskripsi.update-informasi');

    Route::get('management/kirim-chip', 'DeskripsiController@indexKirimChip')->name('deskripsi.index.kirim-chip');
    Route::get('management/tukar-wbz-stockroom', 'DeskripsiController@indexKirimChip')->name('deskripsi.index.tukar-wbz-stockroom');

	Route::get('riwayat-topup', 'RiwayatController@topup')->name('riwayat-topup');
	Route::get('riwayat-jual-chip', 'RiwayatController@jualchip')->name('riwayat-jual-chip');
	Route::get('riwayat-kirim-chip', 'RiwayatController@kirimChip')->name('riwayat-kirim-chip');
	Route::get('riwayat-bonus-sponsor', 'RiwayatController@bonusSponsor')->name('riwayat-bonus-sponsor');

	Route::get('users', 'UserController@index')->name('user');
	Route::get('user/{id}', 'UserController@show')->name('user.show');

    Route::get('reset-password-member', 'ResetPasswordMemberController@index')->name('reset-password-member');
    Route::post('reset-password-member/{token}/update', 'ResetPasswordMemberController@update')->name('reset-password-update');

    Route::group(['namespace' => 'Store'], function (){
        Route::get('saldo-wbz', 'SaldowbzController@index')->name('saldo-wbz.index');
        Route::post('saldo-wbz/{id}/update', 'SaldowbzController@store')->name('saldo-wbz.store');
        Route::get('saldo-wbz/{id}/destroy', 'SaldowbzController@destroy')->name('saldo-wbz.destroy');

        Route::get('saldo-wbz-chip', 'SaldoChipController@index')->name('saldo-wbz-chip.index');
        Route::post('saldo-wbz-chip/{id}/update', 'SaldoChipController@store')->name('saldo-wbz-chip.store');
        Route::get('saldo-wbz-chip/{id}/destroy', 'SaldoChipController@destroy')->name('saldo-wbz-chip.destroy');

        Route::get('kirim-wbz', 'KirimSaldowbzController@index')->name('kirim-wbz.index');
        Route::post('kirim-wbz/{id}/update', 'KirimSaldowbzController@store')->name('kirim-wbz.store');
        Route::get('kirim-wbz/{id}/destroy', 'KirimSaldowbzController@destroy')->name('kirim-wbz.destroy');

        Route::get('kirim-wbz-member', 'KirimSaldowbzMemberController@index')->name('kirim-wbz-member.index');
        Route::post('kirim-wbz-member/store', 'KirimSaldowbzMemberController@store')->name('kirim-wbz-member.store');

        Route::get('produk', 'ProdukController@index')->name('produk.index');
        Route::get('produk/create', 'ProdukController@create')->name('produk.create');
        Route::post('produk/store', 'ProdukController@store')->name('produk.store');
        Route::get('produk/edit/{id}', 'ProdukController@edit')->name('produk.edit');
        Route::post('produk/update/{id}', 'ProdukController@update')->name('produk.update');
        Route::get('produk/detail/{id}', 'ProdukController@detailImage')->name('produk.detailImage');
        Route::post('produk/detail/image/{id}', 'ProdukController@storeDetailProduk')->name('produk.storeDetailProduk');
        Route::get('produk/detail/image/{id}/delete', 'ProdukController@destroyDetailProduk')->name('produk.destroyDetailProduk');
        Route::get('produk/{id}/destroy', 'ProdukController@destroy')->name('produk.destroy');

        Route::get('pemesanan', 'PemesananController@index')->name('pemesanan.index');
        Route::get('pemesanan/show/{id}', 'PemesananController@show')->name('pemesanan.show');
        Route::post('pemesanan/update/{id}', 'PemesananController@update')->name('pemesanan.update');

        Route::get('messages', 'MessagesController@index')->name('messages.index');
        Route::get('message/{user_id}', 'MessagesController@show')->name('messages.show');
        Route::post('message/{user_id}/store', 'MessagesController@store')->name('messages.store');

        Route::get('riwayat-wbzstockroom', 'RiwayatStoreController@wbzstockroom')->name('riwayat-wbzstockroom');
        Route::get('riwayat-wbz-chip', 'RiwayatStoreController@wbzchip')->name('riwayat-chip-wbz');
        Route::get('riwayat-kirim-wbz', 'RiwayatStoreController@kirimwbzstockroom')->name('riwayat-kirim-wbzstockroom');
        Route::get('riwayat-kirim-wbz-member', 'RiwayatStoreController@kirimMemberwbzstockroom')->name('riwayat-kirim-member-wbzstockroom');
    });
});